﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryButton : MonoBehaviour , ICategoryButton{

    public Category Category { get; set; }

    public void Pressed () {
        MenuTypeLoader typeLoader = FindObjectOfType<MenuTypeLoader>();
        typeLoader.LoadCategory(Category);
    }

    public void Setup(Category c) {
        GetComponent<Image>().sprite = GameManager.instance.WorksheetListManager.GetCategoryImage(c.Image);
        Category = c;
    }
}
