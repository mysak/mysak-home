﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface tlačítka kategorie
/// </summary>
public interface ICategoryButton {
    /// <summary>
    /// Inicializace tlačítka
    /// </summary>
    void Setup(Category c);

    /// <summary>
    /// Tlačítko stisknuté
    /// </summary>
    void Pressed();
}
