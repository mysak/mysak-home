﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TypeButton : MonoBehaviour, ITypeButton {

    List<Worksheet> worksheets;
    public Type Type { get; set; }

    /// <summary>
    ///  Náhodný generátor čísla. Pokud je generátor používán často po sobě, 
    /// je potřeba udržovat si jen jednu instanci, jinak může generovat pořád stejná čísla.
    /// </summary>
    protected static System.Random rnd;

    /// <summary>
    /// Inicializuje náhodný generátor, pokud ještě nebyl generován.
    /// </summary>
    void Awake() {
        if (rnd == null) {
            rnd = new System.Random();
        }
        DontDestroyOnLoad(gameObject);
    }
    
    public void Pressed() {
        if (Type.worksheetIds.Count > 0) {
            SceneManager.LoadScene(Type.worksheetIds[rnd.Next(0, Type.worksheetIds.Count)]);
        }
    }

    public void Setup(Type type) {
        GetComponent<Image>().sprite = GameManager.instance.WorksheetListManager.GetTypeImage(type.Image);
        Type = type;
    }
}
