﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GateButton : MonoBehaviour {
    /// <summary>
    ///  Náhodný generátor čísla. Pokud je generátor používán často po sobě, 
    /// je potřeba udržovat si jen jednu instanci, jinak může generovat pořád stejná čísla.
    /// </summary>
    protected static System.Random rnd;

    /// <summary>
    /// Inicializuje náhodný generátor, pokud ještě nebyl generován.
    /// </summary>
    void Awake() {
        if (rnd == null) {
            rnd = new System.Random();
        }
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Vybere náhodný worksheet a spustí jeho scénu.
    /// </summary>
    public void LoadRandomWorksheet() {
        Debug.Log("Random worksheet");        
        SceneManager.LoadScene(rnd.Next(1, GameManager.instance.WorksheetListManager.LoadWorksheets().Count + 1).ToString());        
    }
}
