﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimePlayedControl : MonoBehaviour {

    public AudioClip sound;

	/// <summary>
    /// Při načtení scény zkontroluje, jestli uživatel nepřesáhl denní časový limit.
    /// Pokud se uživatel přihlásil poprvé, přehraje mu instrukce pro výběr levelu.
    /// </summary>
	void Awake () {
        UserData data = GameManager.instance.UserData;
        UserProfile profil = GameManager.instance.UserProfile;

        Debug.Log("Time limit control, time played: " + data.TimeToday + " , day limit: " + profil.DayLimit + 
            " , LastPlayed: " + data.LastPlayed + " , Date: " + DateTime.Now);

        if (data.LastPlayed.Date == DateTime.Now.Date || profil.DayLimit == 0) {
            if (data.TimeToday >= profil.DayLimit || profil.DayLimit == 0) {
                Debug.Log("Time limit exceeded.");
                UnityEngine.SceneManagement.SceneManager.LoadScene("TimeLimitExceeded");
                return;
            }
        }

        if (data.TimeToday == 0) {
            Debug.Log("First log in, playing instructions.");
            SoundManager.instance.PlaySingle(sound);
        }

	}
	

}
