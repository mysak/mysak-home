﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuTypeLoader : MonoBehaviour {

    public GameObject prefab;

    /// <summary>
    /// Načte typy kategorie dané parametrem
    /// </summary>
    /// <param name="category"></param>
    public void LoadCategory(Category category) {
        foreach (Transform child in transform) {
            Destroy(child.gameObject);
        }
        foreach (var type in category.typeIds) {

            GameObject button = Instantiate(prefab);

            ITypeButton typeButton = button.GetComponent<ITypeButton>();
            typeButton.Setup(GameManager.instance.WorksheetListManager.LoadType(type));
            button.transform.SetParent(gameObject.transform, false);
        }
    }
}
