﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface tlačítka typu
/// </summary>
public interface ITypeButton {
    /// <summary>
    /// Inicializace tlačítka
    /// </summary>
    void Setup(Type t);
}
