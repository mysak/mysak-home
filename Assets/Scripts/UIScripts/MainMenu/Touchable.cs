﻿// Potřebné pro funkci neviditelného tlačítka
// zdroj: https://answers.unity.com/questions/801928/46-ui-making-a-button-transparent.html

// Touchable component
using UnityEngine;
using UnityEngine.UI;

public class Touchable : Text {
    protected override void Awake() {
        base.Awake();
    }
}