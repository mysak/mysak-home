﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuCategoryLoader : MonoBehaviour {

    public GameObject prefab;

    /// <summary>
    /// Načte všechny dostupné kategorie
    /// </summary>
    void Awake() {
        var categories = GameManager.instance.WorksheetListManager.LoadCategories();
        foreach (var c in categories.Values) {
            GameObject button = Instantiate(prefab);
            ICategoryButton categoryButton = button.GetComponent<ICategoryButton>();
            categoryButton.Setup(c);
            button.transform.SetParent(gameObject.transform, false);
        }
    }
}
