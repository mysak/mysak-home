﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProfileButton: MonoBehaviour {

    public UserProfile profile { get; set; }

    /// <summary>
    /// Tato metoda je zavolaná při stisku obrázku uživatele. Přihlásí uživatele a načte main menu
    /// </summary>
    public void Pressed () {
        GameManager.instance.UserProfile = profile;
        GameManager.instance.UserData = GameManager.instance.UserManager.LoadUserData(profile.Id);
        Debug.Log("Logging in:" + profile.Id);
        GameManager.instance.LoadNextWorksheet();
    }
}
