﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateUserList : MonoBehaviour {
    public GameObject prefab;

    /// <summary>
    /// Načte seznam uživatelů a dá je jako děti gameobjectu
    /// </summary>
    void Awake() {
        List<UserProfile> profiles = GameManager.instance.UserManager.LoadProfiles();
        foreach (var p in profiles) {
            GameObject button = Instantiate(prefab);

            button.GetComponent<Image>().sprite = GameManager.instance.UserManager.GetProfileImage(p.ImageFile);
            button.GetComponent<ProfileButton>().profile = p;
            button.transform.SetParent(gameObject.transform, false);
            Debug.Log(p.ImageFile);
        }
    }
}
        
