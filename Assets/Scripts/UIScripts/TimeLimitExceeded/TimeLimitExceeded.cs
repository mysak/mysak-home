﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeLimitExceeded : MonoBehaviour {

    /// <summary>
    /// Hláška oznamující, že hráč překročil svůj denní časový limit.
    /// </summary>
    public AudioClip sound;

	/// <summary>
    /// Při načtení scénu přehraje hlášku, která hráči oznamujue, že překročil svůj denní časový limit.
    /// </summary>
	void Awake () {
        SoundManager.instance.PlaySingle(sound);
	}
}
