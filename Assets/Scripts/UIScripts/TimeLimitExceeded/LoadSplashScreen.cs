﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSplashScreen : MonoBehaviour {

    /// <summary>
    /// Při kliknutí na obrazovku se nahraje úvodní obrazovka.
    /// </summary>
	public void LoadScreen() {
        SceneManager.LoadScene("SplashScreen");
    }
}
