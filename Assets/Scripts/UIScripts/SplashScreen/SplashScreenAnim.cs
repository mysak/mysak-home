﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreenAnim : MonoBehaviour {
    public Animator animator;
    public SplashScreenMouse mouse;
   
    private void mouseChange() {
        mouse.ChangeAnim();
    }

	public void ChangeAnimation() {
        animator.SetBool("ChangeButtonAnim", true);
    }
}
