﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreenMouse : MonoBehaviour
{
    public Animator animator;

    //SplashScreenMouse() { }

    public void ChangeAnim()
    {
        animator.SetBool("ChangeMouseAnim", true);
    }
}
