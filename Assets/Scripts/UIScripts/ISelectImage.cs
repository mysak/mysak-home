﻿/// <summary>
/// Interface pro třídu, která implementuje volbu obrázku
/// </summary>
public interface ISelectImage {
    /// <summary>
    /// Byl zvolen obrázek zadaný parametrem
    /// </summary>
    void ImageSelected(string image);
}
