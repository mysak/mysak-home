﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButtonUI : MonoBehaviour {

    public string sceneName = "UserSelect";

    public void Exit() {
        SoundManager.instance.StopSound();
        GameManager.instance.UserProfile = null;
        LoadScene loader = new LoadScene();
        loader.ChangeScene(sceneName);
    }

}
