﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ConfirmDialog : MonoBehaviour {

    public Text Text;
    public Button YesButton;
    public Button NoButton;

    private UnityAction YesAction;
    private UnityAction NoAction;

    /// <summary>
    /// Nastavení potvrzovacího dialogu
    /// </summary>
    /// <param name="text">Zobrazovaný text</param>
    /// <param name="yesAction">Akce při stisknu ano</param>
    /// <param name="noAction">Akce při stisku ne</param>
    public void Setup (string text, UnityAction yesAction, UnityAction noAction) {
        Text.text = text;
        YesAction = yesAction;
        NoAction = noAction;
        YesButton.onClick.AddListener(YesAction);
        YesButton.onClick.AddListener( () => Destroy(gameObject));
        NoButton.onClick.AddListener(() => Destroy(gameObject));
    }
}
