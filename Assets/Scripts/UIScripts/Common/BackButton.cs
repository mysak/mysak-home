﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour {
    public bool Quit;
    public string Scene = "quit";
	/// <summary>
    /// Kontroluje, zda nebylo zmáčnuté tlačítko zpět
    /// </summary>
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
            GoBack();
	}

    /// <summary>
    /// Tlačítko zpět bylo zmáčkunto
    /// </summary>
    private void GoBack() {
        if (Quit)
            Application.Quit();
        SceneManager.LoadScene(Scene);
    }
}
