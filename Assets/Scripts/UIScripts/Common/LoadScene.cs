﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

    /// <summary>
    /// Načte scénu zadanou parametrem
    /// </summary>
    /// <param name="scene">Scéna, která se načte</param>
    public void ChangeScene(string scene) {
        SceneManager.LoadScene(scene);
    }
}
