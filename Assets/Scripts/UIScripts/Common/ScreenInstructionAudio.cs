﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Slouží k přehrání instrukcí scény.
/// </summary>
public class ScreenInstructionAudio : MonoBehaviour {
    /// <summary>
    /// Nahrávka obsahující instrkce.
    /// </summary>
    public AudioClip sound;
    /// <summary>
    /// Zdroj zvuku. Nevyužívá SoundManageru, aby instrikce nepřesahovaly do ostatních scén.
    /// </summary>
    public AudioSource source;

	/// <summary>
    /// Při načtení scény se přehrají instrukce.
    /// </summary>
	void Awake () {
        source.clip = sound;
        source.Play();
	}
}
