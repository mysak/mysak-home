﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageFinderButton : MonoBehaviour {

    public ISelectImage parentView { get; set; }
    public string Image { get; set; }


    /// <summary> Nastaví referenci na nadřazený skript </summary>
    public void Start() {
        parentView = GetComponentInParent<ISelectImage>();
    }
    public void Setup (Sprite image) {
        GetComponent<Image>().sprite = image;
        Image = image.name;
    }

    /// <summary>
    /// Nastaví zvolený obrázek na obrázek reprezentovaný tímto tlačítkem
    /// </summary>
    public void SetImage() {
        parentView.ImageSelected(Image);
    }
}

