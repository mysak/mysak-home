﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageFinder : MonoBehaviour {

    public GameObject ImagePrefab;
    public string ImageFolder;


    /// <summary>
    /// Načte složku s obrázky a vykreslí je na obrazovku. Přidá obrázky jako dítě gameobjectu.
    /// </summary>
    /// <param name="folder"></param>
    public void Setup(string folder) {
        Debug.Log(ImageFolder);
        ImageFolder = folder;
        Sprite[] sprites = Resources.LoadAll<Sprite>(ImageFolder);
        foreach (var sprite in sprites) {
            GameObject ImageView = Instantiate(ImagePrefab);
            ImageView.GetComponent<ImageFinderButton>().Setup(sprite);
            ImageView.transform.SetParent(transform);
        }
    }
}
