﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioInvite : MonoBehaviour {

    /// <summary>
    /// Hláška vyzývající hráče ke hře scénáře
    /// </summary>
    public AudioClip sound;

    /// <summary>
    /// Při načtení se přehraje daná hláška
    /// </summary>
    void Awake() {
        SoundManager.instance.PlaySingle(sound);
    }
}
