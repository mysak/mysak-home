﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenarioStart : MonoBehaviour {

    /// <summary>
    /// Po kliknutí na obrazovku se načte ze scénáře worksheet, který je právě na řadě
    /// </summary>
	public void LoadFirstWorksheet() {
        GameManager.instance.LoadNextWorksheet();
    }
}
