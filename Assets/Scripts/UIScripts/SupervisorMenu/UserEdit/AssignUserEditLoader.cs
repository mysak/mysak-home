﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssignUserEditLoader : MonoBehaviour {

    public GameObject prefab;

    /// <summary>
    /// Načte všechny dostupné scénáře
    /// </summary>
    void Awake() {
        List<Scenario> Scenarios = GameManager.instance.ScenarioManager.LoadScenarios();
        foreach(var p in Scenarios) { 
            GameObject button = Instantiate(prefab);

            ScenarioAssignButton AssignScenarioButton = button.GetComponent<ScenarioAssignButton>();
            AssignScenarioButton.Setup(p);
            button.transform.SetParent(gameObject.transform, false);
        }
    }
}
