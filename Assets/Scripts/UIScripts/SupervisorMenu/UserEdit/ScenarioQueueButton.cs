﻿using UnityEngine;
using UnityEngine.UI;

public class ScenarioQueueButton : QueueButton<Scenario> {

    public Text ScenarioName;

    /// <summary>
    /// Inicializace tlačítka ve frontě scénářů uživatele
    /// </summary>
    override public void Setup (Scenario s, QueueLoader<Scenario> qLoader) {
        base.Setup(s, qLoader);
        ScenarioName.text = s.Name;
    }
}
