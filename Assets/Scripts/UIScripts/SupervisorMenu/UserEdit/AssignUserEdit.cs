﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignUserEdit : MonoBehaviour {

    public UserProfile UserProfile { get; set; }
    public UserData UserData { get; set; }
    public ScenarioQueueLoader QueueLoader;

    /// <summary>
    /// Inicializace menu pro přiřazení scénářů uživateli
    /// </summary>
    public void Setup(UserProfile user) {
        UserProfile = user;
        UserData = GameManager.instance.UserManager.LoadUserData(user.Id);
        List<Scenario> scenarios = new List<Scenario>();
        foreach (var s in UserData.Scenarios) {
            scenarios.Add(GameManager.instance.ScenarioManager.LoadScenario(s));
        }
        QueueLoader.Setup(scenarios);
    }

    /// <summary>
    /// Uložení fronty scénářů uživatele
    /// </summary>
    public void Save() {
        Queue<long> ScenarioIds = new Queue<long>();
        foreach (var o in QueueLoader.Objects)
            ScenarioIds.Enqueue(o.Id);
        UserData.Scenarios = ScenarioIds;
        GameManager.instance.UserManager.SaveUserData(UserData, UserProfile.Id);
        Destroy(gameObject);
    }

    /// <summary>
    /// Zruší upravování scénáře a zavře menu
    /// </summary>
    public void Cancel() {
        Destroy(gameObject);
    }
}
