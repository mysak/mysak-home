﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UserEditButton : MonoBehaviour {

    public Text UserName;
    public Image UserImage;
    public Button Delete;
    public Button ScenarioQueue;
    public GameObject EditMenu;
    public GameObject AssignMenu;
    public GameObject ConfirmDialog;

    public UserProfile UserProfile {get; set;}

    /// <summary>
    /// Inicializace tlačítka uživatele
    /// </summary>
    public void Setup(UserProfile user) {
        UserProfile = user;
        UserImage.sprite = GameManager.instance.UserManager.GetProfileImage(user.ImageFile);
        UserName.text = user.Name;
    }

    /// <summary>
    /// Zobrazí dialog pro smazání profilu a pokud uživatel zvolí ano, smaže profil
    /// </summary>
    public void DeleteProfile() {
        ConfirmDialog dialog = Instantiate(ConfirmDialog).GetComponent<ConfirmDialog>();
        dialog.transform.SetParent(transform.root, false);
        dialog.Setup("Opravdu chcete smazat profil?", () => {
            GameManager.instance.UserManager.DeleteUser(UserProfile.Id);
            Destroy(gameObject);
        }, () => {} );
    }

    /// <summary>
    /// Zobrazí menu pro úpravu profilu
    /// </summary>
    public void EditProfile() {
        Debug.Log("Edit pressed");
        GameObject editmenu = Instantiate(EditMenu);
        editmenu.GetComponent<UserEditProfile>().Setup(UserProfile);
        editmenu.transform.SetParent(transform.root, false);
    }

    /// <summary>
    /// Zobrazí menu pro přiřazení scénářů uživateli
    /// </summary>
    public void AssignScenarios() {
        GameObject assignMenu = Instantiate(AssignMenu);
        assignMenu.GetComponent<AssignUserEdit>().Setup(UserProfile);
        assignMenu.transform.SetParent(transform.root, false);
    }


}
