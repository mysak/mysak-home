﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserEditProfile : MonoBehaviour, ISelectImage {

    public Image UserImage;
    public InputField Name;
    public Slider PlayTimeSlider;
    public Text PlayTimeText;
    public GameObject ImageFinderPrefab;

    public UserProfile UserProfile {get; set;}
    private int TimeValue;

    /// <summary>
    /// Inicializace menu pro úpravu profilu
    /// </summary>
    public void Setup(UserProfile user) {
        UserProfile = user;
        if (user.Id == 0)
            UserImage.sprite = GameManager.instance.UserManager.GetProfileImage("penguin");
        else
            UserImage.sprite = GameManager.instance.UserManager.GetProfileImage(user.ImageFile);
        Name.text = user.Name;
        PlayTimeSlider.value = user.DayLimit / 15;
        SetPlayTimeValue();
    }

    /// <summary>
    /// Uloží upravený profil
    /// </summary>
    public void Save() {
        if (UserProfile.Id == 0)
            GameManager.instance.UserManager.CreateProfile(new UserProfile(0, Name.text, TimeValue, UserImage.sprite.name));
        else
            GameManager.instance.UserManager.SaveProfile(new UserProfile(UserProfile.Id, Name.text, TimeValue, UserImage.sprite.name));
        SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
    }

    /// <summary>
    /// Zruší úpravu profilu a zavře menu
    /// </summary>
    public void Cancel() {
        Destroy(gameObject);
    }

    /// <summary>
    /// Nastaví časový limit uživatele na hodnotu, která je na slideru
    /// </summary>
    public void SetPlayTimeValue() {
        TimeValue = (int)PlayTimeSlider.value * 15;
        PlayTimeText.text = (TimeValue / 60).ToString() + "h " + (TimeValue % 60).ToString("D2") + "m";
    }

    /// <summary>
    /// Tato metoda se volá poté, co si uživatel zvolí obrázek, ten se nastaví do jeho profilu
    /// </summary>
    /// <param name="image"></param>
    public void ImageSelected(string image) {
        UserImage.sprite = GameManager.instance.UserManager.GetProfileImage(image);
        Destroy(transform.Find("ImageFinder").gameObject);
    }

    /// <summary>
    /// Zobrazí menu pro výběr obrázku
    /// </summary>
    public void SelectImage() {
        GameObject imageFinder = Instantiate(ImageFinderPrefab);
        imageFinder.name = "ImageFinder";
        imageFinder.transform.GetComponentInChildren<ImageFinder>().Setup("Sprites/ProfileImages/");
        imageFinder.transform.SetParent(transform, false);
    }
}
