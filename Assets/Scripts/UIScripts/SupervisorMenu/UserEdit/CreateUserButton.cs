﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateUserButton : MonoBehaviour {

    public GameObject EditMenu;

    public UserProfile UserProfile {get; set;}

    /// <summary>
    /// Vytvoří profil uživatele a zobrazí menu pro jeho úpravu
    /// </summary>
    public void CreateProfile() {
        UserProfile = new UserProfile(0, "Jméno", 480, "");
        GameObject editmenu = Instantiate(EditMenu);
        editmenu.GetComponent<UserEditProfile>().Setup(UserProfile);
        editmenu.transform.SetParent(transform.root, false);
    }
}
