﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserEditLoader : MonoBehaviour {

    public GameObject prefab;

    /// <summary>
    /// Inicializace, zobrazí seznam všech profilů
    /// </summary>
    void Awake() {
        List<UserProfile> profiles = GameManager.instance.UserManager.LoadProfiles();
        foreach(var p in profiles) { 
            GameObject button = Instantiate(prefab);

            UserEditButton usrEditButton = button.GetComponent<UserEditButton>();
            usrEditButton.Setup(p);
            button.transform.SetParent(gameObject.transform, false);
        }
    }
}
