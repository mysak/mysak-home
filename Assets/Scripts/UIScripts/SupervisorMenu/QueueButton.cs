﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueueButton<T> : MonoBehaviour {

    protected QueueLoader<T> queueLoader;
    protected T Obj;

    public void Awake() {
        queueLoader = GetComponentInParent<QueueLoader<T>>();
    }

    /// <summary>
    /// Inicializace tlačítka fronty
    /// </summary>
    /// <param name="s">objekt tlačítka</param>
    /// <param name="qLoader">QueueLoader, který se stará o zobrazování a načítání fronty</param>
    virtual public void Setup (T s, QueueLoader<T> qLoader) {
        Obj = s;
        queueLoader = qLoader;
    }
    
    /// <summary>
    /// Smazání této položky fronty
    /// </summary>
    public void Delete() {
        queueLoader.Delete(Obj);
        Destroy(gameObject);
    }

    /// <summary>
    /// Posunutí této položky o 1 nahoru
    /// </summary>
    public void Up() {
        int index = transform.GetSiblingIndex();
        if (index <= 0)
            return;
        queueLoader.MoveUp(index);
        transform.SetSiblingIndex(index - 1);
    }

    /// <summary>
    /// Posunutí této položky o 1 dolu
    /// </summary>
    public void Down() {
        int index = transform.GetSiblingIndex();
        if (index >= queueLoader.Objects.Count -1)
            return;
        queueLoader.MoveDown(index);
        transform.SetSiblingIndex(index + 1);
    }

}
