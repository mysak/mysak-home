﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorksheetQueueButton : QueueButton<Worksheet> {

    public Image Image;
    public Text WorksheetName;
    public Text WorksheetDifficulty;

    /// <summary>
    /// Inicializace tlačítka ve frontě worksheetů
    /// </summary>
    override public void Setup (Worksheet w, QueueLoader<Worksheet> qLoader) {
        base.Setup(w, qLoader);
        Image.sprite = GameManager.instance.WorksheetListManager.GetWorksheetImage(w.Image);
        WorksheetDifficulty.text = w.Difficulty.ToString();
        WorksheetName.text = w.Name;
    }

}
