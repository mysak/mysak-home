﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateScenarioButton : MonoBehaviour {

    public GameObject EditMenu;

    /// <summary>
    /// Tlačítko na vytvoření scénáře, spustí menu editace scénáře
    /// </summary>
    public void CreateScenario() {
        GameObject editmenu = Instantiate(EditMenu);
        editmenu.GetComponent<ScenarioEdit>().Setup(new Scenario(new List<string>(), 0, "Vložte název"));
        editmenu.transform.SetParent(transform.root, false);
    }
}
