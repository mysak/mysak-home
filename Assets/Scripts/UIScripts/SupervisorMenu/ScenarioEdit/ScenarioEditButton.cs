﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScenarioEditButton : MonoBehaviour {

    public Text ScenarioName;
    public Button Delete;
    public Button UserQueue;
    public GameObject EditMenu;
    public GameObject AssignMenu;
    public ConfirmDialog ConfirmDialog;

    public Scenario Scenario {get; set;}

    /// <summary>
    /// Inicializace tlačítka
    /// </summary>
    /// <param name="scen"></param>
    public void Setup(Scenario scen) {
        Scenario = scen;
        ScenarioName.text = Scenario.Name;
    }

    /// <summary>
    /// Smaže scénář
    /// </summary>
    public void DeleteScenario() {
        ConfirmDialog dialog = Instantiate(ConfirmDialog).GetComponent<ConfirmDialog>();
        dialog.transform.SetParent(transform.root, false);
        dialog.Setup("Opravdu chcete smazat scénář?", () => {
            GameManager.instance.ScenarioManager.DeleteScenario(Scenario.Id);
            Destroy(gameObject);
        }, () => {} );
    }

    /// <summary>
    /// Zobrazí menu editace scénářů
    /// </summary>
    public void EditScenario() {
        GameObject editmenu = Instantiate(EditMenu);
        editmenu.GetComponent<ScenarioEdit>().Setup(Scenario);
        editmenu.transform.SetParent(transform.root, false);
    }

    /// <summary>
    /// Zobrazí menu přiřazování scénářů uživatelům
    /// </summary>
    public void AssignToUsers () {
        GameObject assignMenu = Instantiate(AssignMenu);
        assignMenu.GetComponent<AssignScenarioEdit>().Setup(Scenario);
        assignMenu.transform.SetParent(transform.root, false);
    }
}
