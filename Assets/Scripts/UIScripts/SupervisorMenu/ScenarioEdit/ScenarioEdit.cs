﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScenarioEdit : MonoBehaviour {
    public InputField Name;
    public WorksheetQueueLoader QueueLoader;

    public Scenario Scenario {get; set;}

    /// <summary>
    /// Inicializace menu pro úpravu scénáře
    /// </summary>
    public void Setup(Scenario scenario) {
        Scenario = scenario;
        Name.text = scenario.Name;
        List<Worksheet> worksheets = new List<Worksheet>();
        foreach (string s in scenario.Worksheets)
            worksheets.Add(GameManager.instance.WorksheetListManager.LoadWorksheet(s));

        QueueLoader.Setup(worksheets);
    }

    /// <summary>
    /// Uloží scénář a zavře menu
    /// </summary>
    public void Save() {
        List<string> WorksheetIds = new List<string>();
        foreach (var w in QueueLoader.Objects)
            WorksheetIds.Add(w.Id);

        if (Scenario.Id == 0)
            GameManager.instance.ScenarioManager.CreateScenario(new Scenario(WorksheetIds, 0, Name.text));
        else
            GameManager.instance.ScenarioManager.SaveScenario(new Scenario(WorksheetIds, Scenario.Id, Name.text));
        SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
    }

    /// <summary>
    /// Zruší editaci a zavře menu
    /// </summary>
    public void Cancel() {
        Destroy(gameObject);
    }

}
