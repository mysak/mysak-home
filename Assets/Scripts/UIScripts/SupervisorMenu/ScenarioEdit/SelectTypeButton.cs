﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectTypeButton : MonoBehaviour , ITypeButton{

    public Type Type { get; set; }
    public Image Image;
    public Text Text;


    /// <summary>
    /// Inicializace tlačítka pro výběr typu
    /// </summary>
    public void Setup (Type type) {
        Image.sprite = GameManager.instance.WorksheetListManager.GetTypeImage(type.Image);
        Text.text = type.Name;
        Type = type;
    }

    /// <summary>
    /// Načte worksheety, které spadají do daného typu
    /// </summary>
    public void Pressed() {
        WorksheetLoader worksheetLoader = FindObjectOfType<WorksheetLoader>();
        worksheetLoader.LoadType(Type);
    }
}
