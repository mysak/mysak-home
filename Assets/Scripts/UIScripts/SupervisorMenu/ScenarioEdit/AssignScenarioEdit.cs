﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignScenarioEdit : MonoBehaviour {

    public AssignScenarioEditLoader AssignEditLoader;
    public Scenario Scenario { get; set; }

    /// <summary>
    /// Inicializace, nastavení scénáře
    /// </summary>
    /// <param name="scenario"></param>
    public void Setup(Scenario scenario) {
        Scenario = scenario;
    }

    /// <summary>
    /// Přiřazení scénáře zaškrtnutým uživatelům
    /// </summary>
    public void Save() {
        foreach (var user in AssignEditLoader.GetAllChecked()) {

            UserData data = GameManager.instance.UserManager.LoadUserData(user.Id);
            if (data.Scenarios == null)
                data.Scenarios = new Queue<long>();
            data.Scenarios.Enqueue(Scenario.Id);
            GameManager.instance.UserManager.SaveUserData(data, user.Id);
            Debug.Log(GameManager.instance.UserManager.LoadUserData(user.Id).Scenarios.Count);
        }
        Destroy(gameObject);
    }

    /// <summary>
    /// Zruší přiřazování a zavře menu
    /// </summary>
    public void Cancel() {
        Destroy(gameObject);
    }
}
