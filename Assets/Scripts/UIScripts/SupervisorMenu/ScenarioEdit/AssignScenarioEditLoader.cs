﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssignScenarioEditLoader : MonoBehaviour {

    public GameObject prefab;

    /// <summary>
    /// Načte profily a zobrazí menu pro výběr profilů, kterým bude přiřazen scénář
    /// </summary>
    void Awake() {
        List<UserProfile> profiles = GameManager.instance.UserManager.LoadProfiles();
        foreach(var p in profiles) { 
            GameObject button = Instantiate(prefab);

            AssignScenarioUserButton assignUserButton = button.GetComponent<AssignScenarioUserButton>();
            assignUserButton.Setup(p);
            button.transform.SetParent(gameObject.transform, false);
        }
    }

    /// <summary>
    /// Vrátí všechny profily, které uživatel zaškrtl
    /// </summary>
    public  List<UserProfile> GetAllChecked() {
        List<UserProfile> result = new List<UserProfile>();
        foreach (Transform child in transform) {
            if (child.GetComponentInChildren<Toggle>().isOn) {
                result.Add(child.GetComponentInChildren<AssignScenarioUserButton>().UserProfile);
            }
        }
        return result;
    }
}
