﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorksheetLoader : MonoBehaviour {

    public GameObject prefab;

    /// <summary>
    /// Načte všechny worksheety typu daného parametrem
    /// </summary>
    public void LoadType(Type type) {
        foreach (Transform child in transform) {
            Destroy(child.gameObject);
        }
        foreach (var worksheet in type.worksheetIds) {

            GameObject button = Instantiate(prefab);

            SelectWorksheetButton worksheetButton = button.GetComponent<SelectWorksheetButton>();
            worksheetButton.Setup(GameManager.instance.WorksheetListManager.LoadWorksheet(worksheet));
            button.transform.SetParent(gameObject.transform, false);
        }
    }
}
