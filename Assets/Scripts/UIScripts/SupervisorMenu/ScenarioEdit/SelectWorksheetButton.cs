﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectWorksheetButton : MonoBehaviour {
    public Image Image;
    public Text Name;
    public Text Difficulty;
    public Worksheet Worksheet { get; set; }
    public QueueLoader<Worksheet> queueLoader;

    /// <summary>
    /// Inicializace tlačítka pro výběr worksheetu
    /// </summary>
    /// <param name="w"></param>
    public void Setup (Worksheet w) {
        queueLoader = FindObjectOfType<QueueLoader<Worksheet>>();
        Worksheet = w;
        Image.sprite = GameManager.instance.WorksheetListManager.GetWorksheetImage(w.Image);
        Name.text = w.Name;
        Difficulty.text = w.Difficulty.ToString();
    }

    /// <summary>
    /// Přidá zvolený worksheet do fronty scénáře
    /// </summary>
    public void Pressed() {
        queueLoader.Add(Worksheet);
    }
}
