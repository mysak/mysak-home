﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssignScenarioUserButton : MonoBehaviour {

    public Text UserName;
    public Image UserImage;
    public Toggle Toggle;

    public UserProfile UserProfile {get; set;}

    /// <summary>
    /// Inicializace tlačítka
    /// </summary>
    public void Setup(UserProfile user) {
        UserProfile = user;
        UserImage.sprite = GameManager.instance.UserManager.GetProfileImage(user.ImageFile);
        UserName.text = user.Name;
    }

}
