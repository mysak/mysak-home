﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioEditLoader : MonoBehaviour {

    public GameObject prefab;

    /// <summary>
    /// Načte seznam všech scénářů 
    /// </summary>
    void Awake() {
        List<Scenario> scenarios = GameManager.instance.ScenarioManager.LoadScenarios();
        foreach(var s in scenarios) { 
            GameObject button = Instantiate(prefab);

            ScenarioEditButton scenarioButton = button.GetComponent<ScenarioEditButton>();
            scenarioButton.Setup(s);
            button.transform.SetParent(gameObject.transform, false);
        }
    }
}
