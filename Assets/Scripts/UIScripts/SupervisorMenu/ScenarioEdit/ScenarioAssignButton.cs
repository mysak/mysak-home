﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScenarioAssignButton : MonoBehaviour {

    public Text Name;
    private QueueLoader<Scenario> queueLoader;

    public Scenario Scenario { get; set; }

    /// <summary>
    /// Inicializace tlačítka
    /// </summary>
    /// <param name="scen"></param>
    public void Setup(Scenario scen) {
        Scenario = scen;
        Name.text = Scenario.Name;
        queueLoader = FindObjectOfType<QueueLoader<Scenario>>();
    }

    /// <summary>
    /// Přiřadí scénář do fronty uživatele
    /// </summary>
    public void Assign() {
        queueLoader.Add(Scenario);
    }
}
