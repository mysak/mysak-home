﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectCategoryButton : MonoBehaviour , ICategoryButton{

    public Category Category { get; set; }
    public Image Image;
    public Text Text;

    /// <summary>
    /// Stiskuté tlačítko kategorie, načte typy, které do ní spadají
    /// </summary>
    public void Pressed () {
        MenuTypeLoader typeLoader = FindObjectOfType<MenuTypeLoader>();
        typeLoader.LoadCategory(Category);
        WorksheetLoader w = FindObjectOfType<WorksheetLoader>();
        foreach (Transform t in w.transform)
            Destroy(t.gameObject);
    }

    /// <summary>
    /// Inicializace tlačítka kategorie
    /// </summary>
    public void Setup(Category c) {
        Image.sprite = GameManager.instance.WorksheetListManager.GetCategoryImage(c.Image);
        Text.text = c.Name;
        Category = c;
    }

}
