﻿using System.Collections.Generic;
using UnityEngine;

public class QueueLoader<T>: MonoBehaviour {

    public GameObject buttonPrefab;

    public List<T> Objects { get; set; }

    /// <summary>
    /// Znovu načte všechny objekty fronty
    /// </summary>
    public void UpdateObjects () {
        foreach (Transform child in transform)
            Destroy(child.gameObject);

        foreach (T s in Objects) {
            GameObject button = Instantiate(buttonPrefab);
            QueueButton<T> queueButton = button.GetComponent<QueueButton<T>>();
            queueButton.Setup(s, this);
            button.transform.SetParent(gameObject.transform, false);
        }
    }

    /// <summary>
    /// Odebere položku fronty zadanou parametrem
    /// </summary>
    public void Delete(T s) {
        Objects.Remove(s);
    }

    /// <summary>
    /// Inicializace fronty, položky zadané parametrem
    /// </summary>
    /// <param name="objects"></param>
    public void Setup (List<T> objects) {
        Objects = objects;
        UpdateObjects();
    }


    /// <summary>
    /// Přidat novou položku zadanou parametrem na konec fronty
    /// </summary>
    /// <param name="s"></param>
    public void Add (T s) {
        GameObject button = Instantiate(buttonPrefab);
        QueueButton<T> qbutton = button.GetComponent<QueueButton<T>>();
        qbutton.Setup(s, this);
        Objects.Add(s);
        button.transform.SetParent(gameObject.transform, false);
    }

    /// <summary>
    /// Posune položku zadanou parametrem o 1 nahoru
    /// </summary>
    public void MoveUp(int idx) {
        T s = Objects[idx];
        Objects.RemoveAt(idx);
        Objects.Insert(idx - 1, s);
    }

    /// <summary>
    /// Posune položku zadanou parametrem o 1 dolů
    /// </summary>
    public void MoveDown(int idx) {
        T s = Objects[idx];
        Objects.RemoveAt(idx);
        Objects.Insert(idx + 1, s);
    }
}
