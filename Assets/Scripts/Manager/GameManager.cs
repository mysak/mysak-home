﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Stará se o chod hry, drží si instance ostatních manažerů
/// </summary>
public class GameManager : MonoBehaviour {
    /// <summary>Instance - singleton návrhový vzor</summary>
    public static GameManager instance;
    /// <summary>Profil uživatele</summary>
    public UserProfile UserProfile { get; set; }
    /// <summary>Uživatelská data, obsahuje rozehrané scénáře</summary>
    public UserData UserData { get; set; }
    /// <summary>Manažer statistik</summary>
    public IStatManager StatManager { get; set; }
    /// <summary>Manažer uživatelů</summary>
    public IUserManager UserManager { get; set; }
    /// <summary>Manažer scénářů</summary>
    public IScenManager ScenarioManager { get; set; }
    /// <summary>Manažer worksheetů</summary>
    public IWorksheetListManager WorksheetListManager { get; set; }

    /// <summary>Inicializace singletonu</summary>
    void Awake() {
        Debug.Log("GameManager awake");
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Uloží uživatelská data.
    /// </summary>
    public void SaveUserData() {
        UserManager.SaveUserData(UserData, UserProfile.Id);
    }

    /// <summary>
    /// Načítá scénu po vybrání profilu. V případě, že je uživateli přiřazený scénář, 
    /// otevře v pořadí ze scénáře následující worksheet, jinak otevře hlavní menu.
    /// </summary>
    public void LoadNextWorksheet() {
        switch(UserData.GetScenarioStatus()) {
            case UserData.ScenarioStatus.New:
                SceneManager.LoadScene("ScenarioStart");
                break;
            case UserData.ScenarioStatus.Running:
                int currentWorksheet = UserData.CurrentScenario.Stats.Count;

                Debug.Log("Current scenario: " + UserData.CurrentScenario.Id + ", Worksheet in a row: " + currentWorksheet);
                Scenario currentScenario = ScenarioManager.LoadScenario(UserData.CurrentScenario.Id, UserData.CurrentScenario.Version);
                SceneManager.LoadScene(currentScenario.Worksheets[currentWorksheet]);
                break;
            default:
                SceneManager.LoadScene("MainMenu");
                break;
        }
    }
}
