﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Načítá kategorie, typy a worksheety
/// </summary>
public interface IWorksheetListManager {

    /// <summary>
    /// Načítá hierarchii worksheetů
    /// </summary>
    Dictionary<string, Category> LoadCategories();
    Dictionary<string, Type> LoadTypes();
    Dictionary<string, Worksheet> LoadWorksheets();

    /// <summary>
    /// Vrátí worksheet s Id zadaným parametrem
    /// </summary>
    Worksheet LoadWorksheet(string id);

    Type LoadType(string id);

    Category LoadCategory(string id);

    Sprite GetCategoryImage(string name);

    Sprite GetTypeImage(string name);

    Sprite GetWorksheetImage(string name);

}
