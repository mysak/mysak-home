﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorksheetListFileManager : MonoBehaviour, IWorksheetListManager {
    public IWorksheetListManager instance;
  //  private WorksheetList WorksheetList;
    public Dictionary<string, Worksheet> Worksheets;
    public Dictionary<string, Type> Types;
    public Dictionary<string, Category> Categories;

    const string typesFile = "types";
    const string categoriesFile = "categories";
    const string worksheetsFile = "worksheets";

    /// <summary>
    /// Načte všechny workheety a informace o kategoriích a typech.
    /// </summary>
    void Awake() {

        if (instance == null)
            instance = this;
        else if (ReferenceEquals(this, instance))
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);

        Worksheets = new Dictionary<string, Worksheet>();
        Types = new Dictionary<string, Type>();
        Categories = new Dictionary<string, Category>();

        TextAsset json = Resources.Load<TextAsset>(worksheetsFile);
        WorksheetList worksheetList = JsonUtility.FromJson<WorksheetList>(json.text);
        foreach(var w in worksheetList.Worksheets) {
            Worksheets.Add(w.Id, w);
        }

        json = Resources.Load<TextAsset>(typesFile);
        TypeList typeList = JsonUtility.FromJson<TypeList>(json.text);
        foreach (var t in typeList.Types) {
            Types.Add(t.Id, t);
        }

        json = Resources.Load<TextAsset>(categoriesFile);
        CategoryList categoryList = JsonUtility.FromJson<CategoryList>(json.text);
        foreach (var c in categoryList.Categories) {
            Categories.Add(c.Id, c);
        }
    }

    public Worksheet LoadWorksheet (string id) {
        Worksheet w = new Worksheet();
        Worksheets.TryGetValue(id, out w);
        return w;
    }

    public Sprite GetCategoryImage(string name) {
        return Resources.Load<Sprite>("Sprites/CategoryImages/" + name);
    }

    public Sprite GetTypeImage(string name) {
        return Resources.Load<Sprite>("Sprites/TypeImages/" + name);
    }

    public Sprite GetWorksheetImage(string name) {
        return Resources.Load<Sprite>("Sprites/WorksheetImages/" + name);
    }

    public Dictionary<string, Category> LoadCategories() {
        return Categories;
    }

    public Dictionary<string, Type> LoadTypes() {
        return Types;
    }

    public Dictionary<string, Worksheet> LoadWorksheets() {
        return Worksheets;
    }

    public Type LoadType(string id) {
        Type t;
        Types.TryGetValue(id, out t);
        return t;
    }

    public Category LoadCategory(string id) {
        Category c;
        Categories.TryGetValue(id, out c);
        return c;
    }
}
