﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Linq;

public class UserFileManager : MonoBehaviour, IUserManager {

    private IUserManager instance;
    private const string profileFileName = "profile.usr";
    private const string dataFileName = "usr.dat";

    void Awake() {
        if (instance == null)
            instance = this;
        else if (ReferenceEquals(this, instance))
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    void Start() { }
    void Update() { }

    private string GetUsrDir(long userId) {
        return FileUtil.CombinePaths(FileUtil.UsrDir, userId.ToString());
    }

    private string GetUsrProfilePath( long userId ) {
        return FileUtil.CombinePaths(GetUsrDir(userId), profileFileName);
    }

    private string GetUsrDataPath(long userId) {
        return FileUtil.CombinePaths(GetUsrDir(userId), dataFileName);
    }

    public void SaveProfile(UserProfile p) {
        FileUtil.SaveFile(p, GetUsrProfilePath(p.Id));
    }

    public UserProfile LoadProfile(long userId) {
        return FileUtil.LoadFile<UserProfile>(GetUsrProfilePath(userId));
    }

    public List<UserProfile> LoadProfiles() {
        string dir = FileUtil.CombinePaths(Application.persistentDataPath, FileUtil.UsrDir);
        Directory.CreateDirectory(dir);
        string[] users = Directory.GetDirectories(dir);
        List<UserProfile> result = new List<UserProfile>();
        foreach (var profile in users){
            long profileId = Convert.ToInt64(Path.GetFileName(profile));
            //todo - uncaught FormatException - invalid filename
            result.Add(FileUtil.LoadFile<UserProfile>(GetUsrProfilePath(profileId)));
        }
        return result;
    }

    public void SaveUserData(UserData d, long userId) {
        FileUtil.SaveFile(d, GetUsrDataPath(userId));
    }

    public UserData LoadUserData(long id) {
        return FileUtil.LoadFile<UserData>(GetUsrDataPath(id));
    }

    public void CreateProfile(UserProfile p) {
        p.Id = FileUtil.GetMaxDirId(FileUtil.UsrDir) + 1;
        SaveProfile(p);
        SaveUserData(new UserData(), p.Id);
    }

    public void DeleteUser(long id) {
        FileUtil.DeleteDir(GetUsrDir(id));
    }

    public Sprite GetProfileImage(string name) {
        return Resources.Load<Sprite>("Sprites/ProfileImages/" + name);
    }

    public void DeleteScenarioFromUserQueues(long scenarioId) {
        var profiles = LoadProfiles();
        foreach (var profile in profiles) {
            var userData = LoadUserData(profile.Id);
            var scenarioQueue = new Queue<long>();
            for (int i = 0; i < userData.Scenarios.Count; ++i) {
                if (userData.Scenarios.ElementAt(i) != scenarioId) {
                    scenarioQueue.Enqueue(userData.Scenarios.ElementAt(i));
                }
            }
            userData.Scenarios = scenarioQueue;
            SaveUserData(userData, profile.Id);
        }
    }


}
