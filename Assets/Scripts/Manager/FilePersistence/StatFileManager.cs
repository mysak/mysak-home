﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatFileManager : MonoBehaviour, IStatManager {

    private static IStatManager instance;

    void Awake() {
        if (instance == null)
            instance = this;
        else if (ReferenceEquals(instance, this))
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Vytvoří cestu ke statistice.
    /// </summary>
    /// <param name="s">Statistika</param>
    /// <returns>Vrací string, který obsahuje cestu ke statistice.</returns>
    private string GetStatPath(Stat s) {
        return FileUtil.CombinePaths(
            FileUtil.StatDir,
            FileUtil.NormalStatDir,
            s.TimeStamp.ToString("yyMMddHHmmss") + ".stat");
    }

    /// <summary>
    /// Vytvoří cestu ke scénářové statistice.
    /// </summary>
    /// <param name="s">Scénářová statistika</param>
    /// <returns>Vrací string, který obsahuje cestu ke scénářové statistice.</returns>
    private string GetStatPath(ScenStat s) {
        return FileUtil.CombinePaths(FileUtil.StatDir,
            FileUtil.ScenStatDir,
            s.TimeStamp.ToString("yyMMddHHmmss") + ".stat");
    }

    /// <summary>
    /// Uloží statistiku a zároveň čas strávený hraním započítá do dnešního času stráveného hraním. 
    /// </summary>
    /// <param name="s"></param>
    public void SaveStat(Stat s) {
        UserData data = GameManager.instance.UserData;
        // Jesliže dnes už hrál, přičti to k jeho času.
        if (data.LastPlayed.Date == DateTime.Now.Date) {
            data.TimeToday += s.TimePlayed;
        }
        // Jinak aktualizuj datum a přiřaď tam čas.
        else {
            data.LastPlayed = DateTime.Now;
            data.TimeToday = s.TimePlayed;
        }
        GameManager.instance.UserManager.SaveUserData(data, s.UserId);
        FileUtil.SaveFile(s, GetStatPath(s));
    }

    /// <summary>
    /// Uloží statistiku scénáře.
    /// </summary>
    /// <param name="s"></param>
    public void SaveScenarioStat(ScenStat s) {
        FileUtil.SaveFile(s, GetStatPath(s));
    }
}