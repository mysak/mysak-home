﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class ScenFileManager : MonoBehaviour, IScenManager{
    private IScenManager instance;

    void Awake() {
        if (instance == null)
            instance = this;
        else if (ReferenceEquals(this, instance))
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    string GetScenPath( long id , long ver) {
        return FileUtil.CombinePaths(FileUtil.ScenarioDir, id.ToString(), ver.ToString() + ".sce");
    }

    string GetScenDir (long id) {
        return FileUtil.CombinePaths(FileUtil.ScenarioDir, id.ToString());
    }

    void Start() { }
    void Update() { }

    public Scenario LoadScenario(long id, long ver) {
        return FileUtil.LoadFile<Scenario>(GetScenPath(id, ver));
    }

    public void SaveScenario(Scenario s) {
        if (s.Version == 0)
            s.Version = Directory.GetFiles(FileUtil.CombinePaths(Application.persistentDataPath, GetScenDir(s.Id))).Length + 1;
        FileUtil.SaveFile(s, GetScenPath(s.Id, s.Version));
    }

    public List<Scenario> LoadScenarios() {
        string dir = FileUtil.CombinePaths(Application.persistentDataPath, FileUtil.ScenarioDir);
        string[] scenarios = Directory.GetDirectories(dir);
        List<Scenario> result = new List<Scenario>();
        foreach (var scenario in scenarios){
            long scenarioId = Convert.ToInt64(Path.GetFileName(scenario));
            result.Add(FileUtil.LoadFile<Scenario>(
                    GetScenPath(scenarioId, Directory.GetFiles(FileUtil.CombinePaths(Application.persistentDataPath, GetScenDir(scenarioId))).Length)));
        }
        return result;
    }

    public void DeleteScenario(long id) {
        FileUtil.DeleteDir(GetScenDir(id));
        GameManager.instance.UserManager.DeleteScenarioFromUserQueues(id);
    }

    public void CreateScenario(Scenario s) {
        s.Id = FileUtil.GetMaxDirId(FileUtil.ScenarioDir) + 1;
        s.Version = 1;
        FileUtil.SaveFile(s, GetScenPath(s.Id, s.Version));
    }

    public Scenario LoadScenario(long id) {
        string path = GetScenPath(id, Directory.GetFiles(FileUtil.CombinePaths(Application.persistentDataPath, GetScenDir(id))).Length);
        return FileUtil.LoadFile<Scenario>(path);
    }
}
