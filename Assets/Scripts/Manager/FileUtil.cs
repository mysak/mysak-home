﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Linq;

/// <summary>
/// Utilita pro ukládání souborů
/// </summary>
public static class FileUtil {
    /// <summary>Adresář statistik</summary>
    public static string StatDir = "stat";
        /// <summary>Adresář statistik volné hry</summary>
        public static string NormalStatDir = "normal";
        /// <summary>Adresář scénářových statistik</summary>
        public static string ScenStatDir = "scenario";

    /// <summary>Cesta k souboru s hierarchií worksheetů</summary>
    public static string WorksheetListPath = "worksheets.json";

    /// <summary>Adresář scénářů</summary>
    public static string ScenarioDir = "scen";
    /// <summary>Adresář uživatelů</summary>
    public static string UsrDir  = "usr";

    /// <summary>
    /// Slouží cesty platformě nezávislým způsobem
    /// </summary>
    /// <param name="paths">seznam cest</param>
    public static string CombinePaths ( params string [] paths) {
        string result = paths[0];
        for (int i = 1; i < paths.Length; ++i)
            result = Path.Combine(result, paths[i]);
        return result;
    }

    /// <summary>
    /// Načte objekt z relativní cesty
    /// </summary>
    /// <param name="path">Relativní cesta k souboru</param>
    /// <returns>Načtený soubor typu T</returns>
    public static T LoadFile<T> (string path) {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(CombinePaths(Application.persistentDataPath, path), FileMode.Open);
        T f = (T)bf.Deserialize(file);
        file.Close();
        return f;
    }

    /// <summary>
    /// Uloží objekt na zadanou cestu
    /// </summary>
    /// <param name="o">Objekt k uložení</param>
    /// <param name="path">Cesta uložení</param>
    public static void SaveFile<T>(T o, string path) {
        BinaryFormatter bf = new BinaryFormatter();
        path = CombinePaths(Application.persistentDataPath, path);
        Directory.CreateDirectory(Path.GetDirectoryName(path));
        FileStream file = File.Open(CombinePaths(Application.persistentDataPath, path), FileMode.OpenOrCreate);
        bf.Serialize(file, o);
        file.Close();
    }

    public static T LoadJSON<T>(string path) {
        string json = File.ReadAllText(CombinePaths(Application.persistentDataPath, path));
        return JsonUtility.FromJson<T>(json);
    }

    public static void SaveJSON<T>(T o, string path) {
        string json = JsonUtility.ToJson(o, true);
        File.WriteAllText(CombinePaths(Application.persistentDataPath, path), json);
    }

    /// <summary>
    /// Smaže Adresář zadaný parametrem i s obsahem
    /// </summary>
    /// <param name="dir">Adresář ke smazání</param>
    public static void DeleteDir(string dir) {
        DeleteDirRec(CombinePaths(Application.persistentDataPath, dir));
    }

    public static long GetMaxDirId(string dir) {
        dir = CombinePaths(Application.persistentDataPath, dir);
        Directory.CreateDirectory(dir);
        string[] dirs = Directory.GetDirectories(dir);
        if (dirs.Length == 0)
            return 0;
        List<long> ids = new List<long>();
        foreach (string str in dirs)
            ids.Add(Convert.ToInt64(Path.GetFileName(str)));
        return ids.Max();
    }

    private static void DeleteDirRec (string dir) {
        string[] files = Directory.GetFiles(dir);
        string[] dirs = Directory.GetDirectories(dir);

        foreach (string file in files) {
            File.SetAttributes(file, FileAttributes.Normal);
            File.Delete(file);
        }

        foreach (string d in dirs) {
            DeleteDirRec(dir);
        }

        Directory.Delete(dir, false);
    }



}
