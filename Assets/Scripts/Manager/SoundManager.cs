﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Třída sound manager, metody Awake, PlaySingle a RandomizeSfx jsou částečně převzaty ze zdroje: https://unity3d.com/learn/tutorials/projects/2d-roguelike-tutorial/audio-and-sound-manager
/// </summary>
public class SoundManager : MonoBehaviour {
    /// <summary>
    /// Zdroj zvuku
    /// </summary>
    public AudioSource efxSource;                   //Drag a reference to the audio source which will play the sound effects.
    /// <summary>
    /// Instance audio manageru. Vždy je jen jedna, jedná se tedy o singleton.
    /// </summary>
    public static SoundManager instance = null;     //Allows other scripts to call functions from SoundManager.   
    /// <summary>
    /// Nejnižší možná výška zvuku. Díky různým výškám zvukům nebudou hlášky znít pořád stejně.
    /// </summary>
    public float lowPitchRange = .95f;              //The lowest a sound effect will be randomly pitched.
    /// <summary>
    /// Nejvyšší možná výška zvuku. Díky různým výškám zvukům nebudou hlášky znít pořád stejně.
    /// </summary>
    public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.
    /// <summary>
    ///  Náhodný generátor čísla. Pokud je generátor používán často po sobě, 
    /// je potřeba udržovat si jen jednu instanci, jinak může generovat pořád stejná čísla.
    /// </summary>
    protected static System.Random rnd;
    /// <summary>
    /// Counter, který v sobě droží, kolik zvuků bylo přehráno od jeho posledního vynulování. Slouží k odhalení, jestli byly přerušeny nahrávky, které se měly přehrát těsně za sebou (využívá Prompter ve skriptu Helper).
    /// </summary>
    public int playCounter = 0;


    /// <summary>
    /// Kontroluje, aby vždy existovala jen jedna instance SoundManageru.
    /// </summary>
    void Awake() {
        Debug.Log("SoundManager awake.");
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        if (rnd == null) {
            rnd = new System.Random();
        }

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }


    //Used to play single sound clips.
    /// <summary>
    /// Přehraje jeden sound clip.
    /// </summary>
    /// <param name="clip">Klic k přehrání.</param>
    /// <returns>Vrací délku přehrávaného klipu ve vteřinách.</returns>
    public float PlaySingle(AudioClip clip) {
        playCounter++;
        Debug.Log("SoundManager: play single.");
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        efxSource.clip = clip;

        //Play the clip.
        efxSource.Play();
        return clip.length;
    }


    //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
    /// <summary>
    /// Přehraje náhodný sound clip z pole sound clipů.
    /// </summary>
    /// <param name="clips"></param>
    /// <returns>Vrací délku přehrávaného klipu ve vteřinách.</returns>
    public float RandomizeSfx(params AudioClip[] clips) {
        playCounter++;
        //Generate a random number between 0 and the length of our array of clips passed in.
        int randomIndex = rnd.Next(0, clips.Length); //Random.Range(0, clips.Length);

        //Choose a random pitch to play back our clip at between our high and low pitch ranges.
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        //Set the pitch of the audio source to the randomly chosen pitch.
        efxSource.pitch = randomPitch;

        //Set the clip to the clip at our randomly chosen index.
        efxSource.clip = clips[randomIndex];

        //Play the clip.
        efxSource.Play();
        Debug.Log("Random sound length: " + clips[randomIndex].length);
        return clips[randomIndex].length;
    }

    public void StopSound() {
        efxSource.Stop();
    }



}