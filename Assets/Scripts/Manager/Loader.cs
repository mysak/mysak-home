﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// Načte potřebné singletony, inicializuje celou hru.
/// </summary>
public class Loader : MonoBehaviour {
    /// <summary>Prefab GameManageru, který se má instancovat</summary>
    public GameObject gameManager;
    /// <summary>Prefab StatManageru, který se má instancovat</summary>
    public GameObject statManager;
    /// <summary>Prefab UserManageru, který se má instancovat</summary>
    public GameObject userManager;
    /// <summary>Prefab ScearioManageru, který se má instancovat</summary>
    public GameObject scenarioManager;
    /// <summary>Prefab WorksheetManageru, který se má instancovat</summary>
    public GameObject worksheetListManager;
    /// <summary>
    /// Prefab SoundManageru, který se má instancovat.
    /// </summary>
    public GameObject soundManager;


    /// <summary>
    /// Inicializuje proměnné a singletony
    /// </summary>
    void Awake() {
        if (GameManager.instance == null)
            Instantiate(gameManager);
        if (GameManager.instance.StatManager == null) {
            GameManager.instance.StatManager = Instantiate(statManager).GetComponent<IStatManager>();
        }
        if (GameManager.instance.UserManager == null) {
            GameManager.instance.UserManager = Instantiate(userManager).GetComponent<IUserManager>();
        }

        if (GameManager.instance.ScenarioManager == null) {
            GameManager.instance.ScenarioManager = Instantiate(scenarioManager).GetComponent<IScenManager>();
        }

        if (GameManager.instance.WorksheetListManager == null) {
            GameManager.instance.WorksheetListManager = Instantiate(worksheetListManager).GetComponent<IWorksheetListManager>();
        }
        if (SoundManager.instance == null) {
            Debug.Log("Instantiate sound manager");
            Instantiate(soundManager);
        }

    }
}