﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rozhraní, které slouží k ukládání a načítání uživatelských profilů a dat
/// </summary>
public interface IUserManager {

    /// <summary>
    /// Založí záznam o uživatelském profilu, vytvoří prázdná UserData
    /// </summary>
    /// <param name="p">profil, který se má uložit</param>
    void CreateProfile(UserProfile p);

    /// <summary>
    /// Uloží profil zadaný parametrem, nezakládá nová uživatelská data
    /// </summary>
    /// <param name="p">Profil, který se má uložit</param>
    void SaveProfile(UserProfile p);

    /// <summary>
    /// Načte profil uživatele s ID zadaným parametrem
    /// </summary>
    /// <param name="id">ID uživatele</param>
    UserProfile LoadProfile(long id);


    /// <summary>
    /// Vrací seznam profilů všech uživatelů
    /// </summary>
    List<UserProfile> LoadProfiles();

    /// <summary>
    /// Uloží uživatelská data, pod ID zadané parametrem
    /// </summary>
    /// <param name="d">Data, co se mají uložit</param>
    /// <param name="id">ID Uživatele</param>
    void SaveUserData(UserData d, long id);

    /// <summary>
    /// Smaže 
    /// </summary>
    /// <param name="id"></param>
    void DeleteUser(long id);

    /// <summary>
    /// Načte userdata uživatele s ID zadané parametrem
    /// </summary>
    /// <param name="id">ID uživatele, které se má uložit</param>
    UserData LoadUserData(long id);

    Sprite GetProfileImage(string name);

    /// <summary>
    /// Odstraní scénář z front uživatelů.
    /// </summary>
    /// <param name="scenarioId">Id scénáře, který chceme odstranit.</param>
    void DeleteScenarioFromUserQueues(long scenarioId);

}