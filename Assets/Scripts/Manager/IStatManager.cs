﻿using System.Collections;
using System.Collections.Generic;


/// <summary>
/// Rozhraní, které slouží k ukládání a načítání scénářů
/// </summary>
public interface IStatManager {

    /// <summary>
    /// Uloží statistiku. Dále čas strávený hraním započítá do času, který dnes hráč strávil hraním.
    /// </summary>
    /// <param name="s">Statistika k uložení</param>
    void SaveStat(Stat s);

    /// <summary>
    /// Uloží scénářovou statistiku.
    /// </summary>
    /// <param name="s">Scénářová statistika k uložení</param>
    void SaveScenarioStat(ScenStat s);

        //todo fix parameter
    }
