﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Předtavuje scénář.
/// </summary>
[Serializable]
public class Scenario  {

    /// <summary>
    /// Seznam a pořadí worksheetů, které jsou ve scénáři.
    /// </summary>
    public List<string> Worksheets { get; set; }
    /// <summary>
    /// Id scénáře.
    /// </summary>
    public long Id { get; set; }
    /// <summary>
    /// Verze scénáře. S výjimkou, kdy má nějaký uživatel rozehranou starou verzi, se vždy zobrazuje nejnovější verze scénáře.
    /// </summary>
    public long Version { get; set; }

    /// <summary>
    /// Jméno scénáře.
    /// </summary>
    public string Name { get; set; }

    public Scenario(List<string> levels, long id, long version, string name) {
        this.Worksheets = levels;
        Id = id;
        Version = version;
        Name = name;
    }

    public Scenario(List<string> levels, long id, string name) {
        this.Worksheets = levels;
        Id = id;
        Version = 0;
        Name = name;
    }

}
