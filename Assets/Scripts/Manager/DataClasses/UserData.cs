﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Slouží k ukládání průběžných dat uživatele.
/// </summary>
[Serializable]
public class UserData {
    /// <summary>
    /// Fronta aktuálně přiřazených scénářů.
    /// </summary>
    public Queue<long> Scenarios { get; set; }
    /// <summary>
    /// Čas, který dnes uživatel strávil hraním. Slouží ke kontrolování, jestli nebyl překročen denní časový limit. Je nutno zkontrolovat, jestli je údaj aktuálním dni pomocí proměnné LastPlayed.
    /// </summary>
    public int TimeToday { get; set; }
    /// <summary>
    /// Kdy byl naposledy uživatel přihlášen do aplikace.
    /// </summary>
    public DateTime LastPlayed { get; set; }
    /// <summary>
    /// Aktuální rozehraný scénář, pokud nějaký je.
    /// </summary>
    public ScenStat CurrentScenario { get; set; }

    /// <summary>
    /// Stav rozehraného scénáře.
    /// None = žádný scénář není přiřazen,
    /// New = právě byl otevřen nový scénář,
    /// Running = Hráč má již rozehraný scénář.
    /// </summary>
    public enum ScenarioStatus { None, New, Running };

    /// <summary>
    /// Kontruktor přijímající data atributů.
    /// </summary>
    /// <param name="scenarios"></param>
    /// <param name="timeToday"></param>
    /// <param name="lastPlayed"></param>
    /// <param name="currentScenario"></param>
    public UserData(Queue<long> scenarios, int timeToday, DateTime lastPlayed, ScenStat currentScenario) {
        Scenarios = scenarios;
        TimeToday = timeToday;
        LastPlayed = lastPlayed;
        CurrentScenario = currentScenario;
    }

    /// <summary>
    /// Vrací stav rozehraného scénáře
    /// </summary>
    /// <returns></returns>
    public ScenarioStatus GetScenarioStatus() {
        if (RetrieveScenario())
            return ScenarioStatus.New;
        else if (CurrentScenario == null)
            return ScenarioStatus.None;
        else
            return ScenarioStatus.Running;
    }

    /// <summary>
    /// Stará se o správné načtení scénáře z fronty.
    /// </summary>
    public bool RetrieveScenario() {
        GameManager gm = GameManager.instance;
        bool ret = false;

        if (CurrentScenario != null
            && gm.ScenarioManager.LoadScenario(
                CurrentScenario.Id,
                CurrentScenario.Version).Worksheets.Count <= CurrentScenario.Stats.Count) {
            Debug.Log("Scenario finished");
            CurrentScenario.Save();
            CurrentScenario = null;
            Scenarios.Dequeue();
            gm.SaveUserData();
        }

        if (CurrentScenario == null && Scenarios.Count != 0) {
            long scenerioId = Scenarios.Peek();
            Scenario nextScenario = GameManager.instance.ScenarioManager.LoadScenario(scenerioId);
            CurrentScenario = new ScenStat(gm.UserProfile.Id, nextScenario);
            ret = true;
        }

        gm.SaveUserData();

        return ret;
    }

    /// <summary>
    /// Kontruktor, který data atributů inicializuje sám. Doporučeno, pokud vytváříte nového uživatele.
    /// </summary>
    public UserData()
    {
        Scenarios = new Queue<long>();
        TimeToday = 0;
        LastPlayed = DateTime.Now;
        CurrentScenario = null;
    }
}