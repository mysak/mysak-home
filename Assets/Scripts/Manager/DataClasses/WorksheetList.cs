﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// List worksheetů.
/// </summary>
[Serializable]
public class WorksheetList {
    /// <summary>
    /// List worksheetů.
    /// </summary>
    public List<Worksheet> Worksheets;
}
