﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Instance statistiky jedné hry jednoho scénáře hraného jedním hráčem.
/// </summary>
[Serializable]
public class ScenStat  {
    /// <summary>
    /// Statistiky worheetů, které jsou ve scénáři obsaženy.
    /// </summary>
    public List<DateTime> Stats { get; set; }
    /// <summary>
    /// Id uživatele, který daný scénář hrál.
    /// </summary>
    public long     UserId { get; set; }
    /// <summary>
    /// Id hraného scénáře.
    /// </summary>
    public long     Id { get; set; }
    /// <summary>
    /// Verze hraného scénáře.
    /// </summary>
    public long     Version { get; set; }
    /// <summary>
    /// Průměrná úspěšnost uživatele napříč worheety v procentech. 
    /// </summary>
    public int      SuccessRate { get; set; }
    /// <summary>
    /// Časová známka obsahující začátek hraní scénáře.
    /// </summary>
    public DateTime TimeStamp { get; set; }

    /// <summary>
    /// Přiřadí ke scénářové statistice statistiku konkrétního worksheetu.
    /// Volá se při každém dokončení worksheetu ve scénáři.
    /// </summary>
    /// <param name="stat">Statistika dokončeného worksheetu</param>
    public void AddStat(Stat stat) {
        Stats.Add(stat.TimeStamp);
        GameManager.instance.SaveUserData();
    }

    /// <summary>
    /// Konstruktor statistiky scénáře.
    /// </summary>
    /// <param name="stats">Seznam statistik worksheetů.</param>
    /// <param name="userId">Id uživatele, který daný scénář hrál.</param>
    /// <param name="scenario">Scénář, kte kterému se statistika vztahuje</param>
    public ScenStat(long userId, Scenario scenario) {
        Stats = new List<DateTime>();
        UserId = userId;
        Id = scenario.Id;
        Version = scenario.Version;
        SuccessRate = 0;
        TimeStamp = DateTime.Now;
    }

    /// <summary>
    /// Uloží statistiku scénáře.
    /// </summary>
    public void Save() {
        UnityEngine.Debug.Log(this);
        GameManager.instance.StatManager.SaveScenarioStat(this);
    }
}
