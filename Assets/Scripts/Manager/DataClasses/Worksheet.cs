﻿using System;
using UnityEngine;

/// <summary>
/// Třída reprezentující jeden worksheet. Worksheet odpovídá jedné scéně v unity.
/// </summary>
[Serializable]
public class Worksheet {
    /// <summary>
    /// Jméno worksheetu, které se zobrazuje dohledu.
    /// </summary>
    public string Name;

    /// <summary>
    /// Obrázek worksheetu.
    /// </summary>
    public string Image;

    /// <summary>
    /// Id worksheetu.
    /// </summary>
    public string Id;


    /// <summary>
    /// Obbtížnost worksheetu.
    /// </summary>
    public int Difficulty;
}