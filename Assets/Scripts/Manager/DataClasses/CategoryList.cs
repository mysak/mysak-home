﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// List typů.
/// </summary>
[Serializable]
public class CategoryList {
    /// <summary>
    /// List worksheetů.
    /// </summary>
    public List<Category> Categories;
}
