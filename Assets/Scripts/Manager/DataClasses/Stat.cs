﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Statistika jednoho worheetu.
/// </summary>
[System.Serializable]
public class Stat  {
    /// <summary>
    /// Id uživatele, který daný worksheet hrál.
    /// </summary>
    public long     UserId { get; set; }
    /// <summary>
    /// Id hraného worheetu.
    /// </summary>
    public string   WorksheetId { get; set; }
    /// <summary>
    /// Časová známka udávající začátek hraní daného worksheetu.
    /// </summary>
    public DateTime TimeStamp { get; set; } //Primary ID
    /// <summary>
    /// Čas strávený uživatelem hraním worksheetu.
    /// </summary>
    public int      TimePlayed { get; set; }
    /// <summary>
    /// Procentuální úspěšnost uživatele v daném worsheetu.
    /// </summary>
    public int      SuccessRate { get; set; }
    /// <summary>
    /// Počet úspěšných pokusů, slouží k vypočtení SuccessRate.
    /// </summary>
    private int successCnt;
    /// <summary>
    /// Počet neúspěšných pokusů, slouží k vypočtení SuccessRate.
    /// </summary>
    private int failCnt;


    /// <summary>
    /// Aktualiyuje success rate a tim played a uloží statistiku.
    /// </summary>
    public void Save() {
        this.ComputeSuccessRate();
        this.ComputeTimePlayed();
        UnityEngine.Debug.Log(this);
        GameManager.instance.StatManager.SaveStat(this);
    }

    /// <summary>
    /// Konstruktor statistiky.
    /// </summary>
    /// <param name="userId">Id uživatele, který daný worksheet hrál.</param>
    /// <param name="worksheetId">Id hraného worheetu.</param>
    /// <param name="timeStamp">Časová známka udávající začátek hraní daného worksheetu.</param>
    /// <param name="timePlayed">Čas strávený uživatelem hraním worksheetu.</param>
    /// <param name="successRate">Procentuální úspěšnost uživatele v daném worsheetu.</param>
    public Stat(long userId, string worksheetId, DateTime timeStamp, int timePlayed, int successRate) {
        this.UserId = userId;
        WorksheetId = worksheetId;
        TimeStamp = timeStamp;
        TimePlayed = timePlayed;
        SuccessRate = successRate;
        successCnt = 0;
        failCnt = 0;
    }

    /// <summary>
    /// Konstruktor statistiky, který si sám zjistí timeStamp.
    /// </summary>
    /// <param name="userId">Id uživatele, který daný worksheet hrál.</param>
    /// <param name="worksheetId">Id hraného worheetu.</param>
    /// <param name="timePlayed">Čas strávený uživatelem hraním worksheetu.</param>
    /// <param name="successRate">Procentuální úspěšnost uživatele v daném worsheetu.</param>
    public Stat(long userId, string worksheetId, int timePlayed, int successRate) {
        this.UserId = userId;
        WorksheetId = worksheetId;
        TimeStamp = DateTime.Now;
        TimePlayed = timePlayed;
        SuccessRate = successRate;
        successCnt = 0;
        failCnt = 0;
    }

    /// <summary>
    /// Konstruktor statistiky. Tento kontruktor používeje pro průběžné počítání successRatu.
    /// </summary>
    /// <param name="userId">Id uživatele</param>
    /// <param name="worksheetId">Id worksheetu</param>
    public Stat(long userId, string worksheetId)
    {
        this.UserId = userId;
        this.WorksheetId = worksheetId;
        this.TimeStamp = DateTime.Now;
        successCnt = 0;
        failCnt = 0;
    }

    /// <summary>
    /// Přidá úspěšný pokus.
    /// </summary>
    /// <param name="weight">Volitelný parametr, udává váhu úspěšného pokusu.</param>
    public void AddSuccess(int weight = 1)
    {        
        this.successCnt += weight;
        UnityEngine.Debug.Log(this);
    }

    /// <summary>
    /// Přidá neúspěšný pokus.
    /// </summary>
    /// <param name="weight">Volitelný parametr, udává váhu neúspěšného pokusu.</param>
    public void AddFail(int weight)
    {
        
        this.failCnt += weight;
        UnityEngine.Debug.Log(this);
    }

    /// <summary>
    /// Vypočte successRate z počtu úspěšných a neúspěšných pokusů.
    /// </summary>
    public void ComputeSuccessRate()
    {
        // Předpokládá, že successCnt + failCnt se na konci rovná maximálnímu možnému skóre.
        SuccessRate = (successCnt * 100) / (successCnt + failCnt);
        UnityEngine.Debug.Log(this);
    }

    /// <summary>
    /// Vypočítá čas strávený hraním worksheetu v minutách (počítáno od začátku vytvoření statistiky).
    /// </summary>
    public void ComputeTimePlayed()
    {
        this.TimePlayed = (int) Math.Ceiling((DateTime.Now - TimeStamp).TotalMinutes);        
    }

    /// <summary>
    /// Debugovací metoda, vytvoří string s atributy statistiky.
    /// </summary>
    /// <returns></returns>
    public override string ToString() {
        return "Stat : [ " + UserId + " " + WorksheetId + " " + TimeStamp.ToString() + " " + TimePlayed.ToString() + " " + SuccessRate.ToString() + " "
            + successCnt + " " + failCnt + " ]";
    }
}
