﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Představuje kategorii worheetů.
/// </summary>
[Serializable]
public class Category {
    /// <summary>
    /// Obrázek, který reprezentuje danou kategorii a zobrazuje se v menu volné hry.
    /// </summary>
    public string Image;
    /// <summary>
    /// Jméno kategorie
    /// </summary>
    public string Name;
    /// <summary>
    /// Typy, které jsou obsaženy v této kategorii (jejich jména).
    /// </summary>
    public List<string> typeIds;
    public string Id;
}
