﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Představuje typ worksheetů.
/// </summary>
[Serializable]
public class Type {
    /// <summary>
    /// Obrázek reprezentující daný typ. Zobrazuje se v menu volné hry.
    /// </summary>
    public string Image;
    /// <summary>
    /// Jméno typu.
    /// </summary>
    public string Name;
    /// <summary>
    /// Seznam worksheetů (jejich id), které patří do daného typu.
    /// </summary>
    public List<string> worksheetIds;
    /// <summary>
    /// Seznam kategorií (id kategorií), do kterých tento typ patří. Kategorii můžeme brát jako tag typu.
    /// </summary>
    public List<string> categoryIds;
    public string Id;
}