﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

/// <summary>
/// Třída obsahující transparentní data uživatele.
/// </summary>
[System.Serializable]
public class UserProfile {
    /// <summary>
    /// Id uživatele.
    /// </summary>
    public long     Id          { get; set; }
    /// <summary>
    /// Username uživatele.
    /// </summary>
    public string   Name        { get; set; }
    /// <summary>
    /// Denní časový limit uživatele v minutách.
    /// </summary>
    public int      DayLimit    { get; set; }
    /// <summary>
    /// Obrázek uživatele, který se zobrazuje ve výběru uživatelů.
    /// </summary>
    public string   ImageFile   { get; set; }

    /// <summary>
    /// Konstruktor uživatelstkého profilu.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    /// <param name="dayLimit"></param>
    /// <param name="image"></param>
    public UserProfile(long id, string name, int dayLimit, string image) {
        Id = id;
        Name = name;
        DayLimit = dayLimit;
        ImageFile = image;
    }

    /// <summary>
    /// Debugovací metoda. Vytvoří string obsahující atributy.
    /// </summary>
    /// <returns></returns>
    public override string ToString() {
        return "[ " + Id + " " + Name + " " + DayLimit.ToString() + " " + ImageFile.ToString() +  " ]";
    }
}
