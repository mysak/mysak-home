﻿using System.Collections.Generic;

/// <summary>
/// Rozhraní, které slouží k ukládání a načítání uživatelských profilů a dat
/// </summary>
public interface IScenManager  {
    /// <summary>Načte scénář s ID a verzí zadanou parametrem</summary>
    /// <param name="id">id scénáře k načtení</param>
    /// <param name="ver">verze scénáře k načtení</param>
    Scenario LoadScenario(long id, long ver);

    /// <summary>Načte scénář s ID a nejnovější verzí</summary>
    /// <param name="id">id scénáře k načtení</param>
    Scenario LoadScenario(long id);

    /// <summary>
    /// Načte nejnovější verzi všech scénářů
    /// </summary>
    List<Scenario> LoadScenarios();

    /// <summary>
    /// Odstraní všechny verze scénáře zadaného parametrem
    /// </summary>
    /// <param name="id"></param>
    void DeleteScenario(long id);


    /// <summary>
    /// Uloží scénář zadaný parametrem
    /// </summary>
    /// <param name="s">Scénář k uložení</param>
    void SaveScenario(Scenario s);


    /// <summary>
    /// Uloží scénář zadaný parametrem
    /// </summary>
    /// <param name="s">Scénář k vytvoření</param>
    void CreateScenario(Scenario s);
}
