﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController3 : WorksheetController {

    private void Awake() {
        Init();
    }

    /// <summary>
    /// Uloží statistiku a ukončí worksheet.
    /// </summary>
    public override void SaveAndExit() {
        StartCoroutine(Finish());
    }

    /// <summary>
    /// Přehraje pochvalu - jelikož tento worksheet nemůže skončit jinak než správným tahem uživatele, a uloží statistiku a uloží worksheet.
    /// </summary>
    /// <returns></returns>
    private IEnumerator Finish() {
        yield return new WaitForSeconds(ScoreSounds.PlayPraise());
        AddSuccess();
        base.SaveAndExit();
    }
}
