﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDrop3 : MonoBehaviour {

    /// <summary>
    /// Určuje, jestli je objekt právě přesouván uživatelem.
    /// </summary>
    private bool clicked = false;
    /// <summary>
    /// Vykresluje obrázek objektu.
    /// </summary>
    private SpriteRenderer sr;
    /// <summary>
    /// Na začátku původní pozice objektu, na který se má objekt vracet, pokus zrovna není přemisťován uživatelem.
    /// </summary>
    private Vector3 origin;
    /// <summary>
    /// Cílová destinace objektu
    /// </summary>
    public GameObject destination;
    /// <summary>
    /// Controller tohoto worksheetu.
    /// </summary>
    public SceneController3 sc;
    /// <summary>
    /// Násobitel času, který určuje, jak rychle se bude objekt přesouvat na svůj origin. Čím větší, tím rychlejší.
    /// </summary>
    private int timeMultiplicator = 10;
    /// <summary>
    /// Určuje, jestli by měl být sento objekt vybrán (umístěn do destination), nebo ne.
    /// </summary>
    public bool select;
    /// <summary>
    /// Slouží ke kontrole, jestli už byl objekt jednou špatně přemístěn, aby se případně jedna chyba nezapočítávala dvakrát.
    /// </summary>
    private bool failed = false;

    // Use this for initialization
    void Start() {
        origin = transform.position;
        sr = GetComponent<SpriteRenderer>();
    }

    /// <summary>
    /// Přesouvá objekt zpět na jeho počátek, pokud je to potřeba.
    /// </summary>
    void Update() {
        if (clicked) {
            Move();
        }
        else {
            transform.position = Vector3.Lerp(transform.position, origin, Time.deltaTime * timeMultiplicator);
        }
    }

    /// <summary>
    /// Pohybuje objektem společně s pohybem prstu hráče.
    /// </summary>
    public void Move() {
        Vector3 mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPoint.z = 0;
        transform.position = Vector3.Lerp(transform.position, mouseWorldPoint, Time.deltaTime * 100);
    }

    /// <summary>
    /// Pokud je kliknuto na objekt, zaznamená se tato informace a dá do přední vrstvy.
    /// </summary>
    void OnMouseDown() {
         clicked = true;
         sr.sortingOrder++;
    }

    /// <summary>
    /// Pokud hráč pustil objekt, zkontroluje se, zda byl umístěn na cílovou destinaci. Pokud ano,
    /// tak se do statistiky započítá správný pokus.
    /// </summary>
    void OnMouseUp() {
            clicked = false;
            sr.sortingOrder--;
            BoxCollider2D collider = destination.GetComponent<BoxCollider2D>();
            if (collider.OverlapPoint(transform.position)) {
                if (select) {
                    Destroy(gameObject);
                    sc.SaveAndExit();                    
                    return;
                }
                else {
                    if (!failed) {
                        failed = true;
                        sc.PlayFail();
                        sc.AddFail();
                    }
                    return;
                }
            }
            collider = transform.GetComponent<BoxCollider2D>();
    }
}
