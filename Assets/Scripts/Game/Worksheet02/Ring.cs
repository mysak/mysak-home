﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Řídící skript ke zvonečku, na který hráč zazvoní, když je s worksheetem hotov.
/// </summary>
public class Ring : MonoBehaviour {
    /// <summary>
    /// Kontroler worksheetu
    /// </summary>
    public SceneController2 wk;

    /// <summary>
    /// Pokud hráč zazvonil n zvoneček, zavolá se ukončovací metoda worksheetu.
    /// </summary>
    void OnMouseUp(){
        Debug.Log("Bell ringed.");
        wk.SaveAndExit();
    }
}
