﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class SceneController2 : WorksheetController {

    private DragAndDrop2[] objects;
    public AudioClip sound;

    /// <summary>
    /// Inicializuje atribut objects na dlaždice, které se nachází ve worksheetu.
    /// </summary>
    private void Awake() {
        objects = (DragAndDrop2[])FindObjectsOfType<DragAndDrop2>();
        Init();
    }

    /// <summary>
    /// Zavolá metodu CallSolution, protože ta umožňuje pozdržet aktuální "vlákno".
    /// </summary>
    public override void SaveAndExit() {        
        Debug.Log("Na tohle si zapomnel.");
        foreach (var o in objects) {
            o.Clickable = false;
        }
        StartCoroutine(CallSolutions());
    }
    
    /// <summary>
    /// Zkontroluje, jestli existuje dlaždice, která měla být přesunuta a přesunuta nebyla. V tom případě přehraje příslušnou hlášku a názorně ukáže, které dlaždice měly být přesunuty.
    /// Poté uloží statistiku a načte další scénu.
    /// </summary>
    /// <returns></returns>
    private IEnumerator CallSolutions() {
        bool fail = false;
        foreach (var o in objects) {
            if (o.select && !o.Destroyed) {
                if (!fail) {
                    fail = true;
                    yield return new WaitForSeconds(SoundManager.instance.PlaySingle(sound));
                }
                o.ReachDestination();
                AddFail();
                yield return new WaitForSeconds(2);
            }
        }

        if (!fail) {
            yield return new WaitForSeconds(ScoreSounds.PlayPraise());
        }

        base.SaveAndExit();
    }

}