﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDrop2 : MonoBehaviour {

    /// <summary>
    /// Určuje, jestli je objekt právě přesouván uživatelem.
    /// </summary>
    private bool clicked = false;
    /// <summary>
    /// Vykresluje obrázek objektu.
    /// </summary>
    private SpriteRenderer sr;
    /// <summary>
    /// Na začátku původní pozice objektu, na který se má objekt vracet, pokus zrovna není přemisťován uživatelem.
    /// </summary>
    private Vector3 origin;
    /// <summary>
    /// Cílová destinace objektu
    /// </summary>
    public GameObject destination;
    /// <summary>
    /// Controller tohoto worksheetu.
    /// </summary>
    public SceneController2 sc;
    /// <summary>
    /// Násobitel času, který určuje, jak rychle se bude objekt přesouvat na svůj origin. Čím větší, tím rychlejší.
    /// </summary>
    private int timeMultiplicator = 10;
    /// <summary>
    /// Určuje, jestli by měl být sento objekt vybrán (umístěn do destination), nebo ne.
    /// </summary>
    public bool select;
    /// <summary>
    /// Slouží ke kontrole, jestli už byl objekt jednou špatně přemístěn, aby se případně jedna chyba nezapočítávala dvakrát.
    /// </summary>
    private bool failed = false;
    /// <summary>
    /// Určuje, jestli už byl objekt, ke kterému je skript přiřazen, zničen.
    /// </summary>
    public bool Destroyed = false;
    /// <summary>
    /// Nahrávka názvů obrázku na dlaždici.
    /// </summary>
    public AudioClip sound;
    /// <summary>
    /// Určuje, jestli je hráči umožněno na objekt kliknout.
    /// </summary>
    public bool Clickable = true;

    // Use this for initialization
    void Start () {
        origin = transform.position;
        sr = GetComponent<SpriteRenderer>();
    }
	
	/// <summary>
    /// Přesouvá objekt zpět na jeho počátek, pokud je to potřeba.
    /// Dále kontroluje, jestli nebyl objekt v rámci nápovědy přemístěn na svou sílovou destinaci,
    /// v takovém případě tento objekt zničí.
    /// </summary>
	void Update () {
        if (clicked) {
            Move();
        }
        else {
            transform.position = Vector3.Lerp(transform.position, origin, Time.deltaTime * timeMultiplicator);

            BoxCollider2D collider = destination.GetComponent<BoxCollider2D>();
            if (Destroyed && collider.OverlapPoint(transform.position)) {
                Destroy(gameObject);                
            }
        }
    }

    /// <summary>
    /// Pohybuje objektem společně s pohybem prstu hráče.
    /// </summary>
    public void Move() {
        Vector3 mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPoint.z = 0;
        transform.position = Vector3.Lerp(transform.position, mouseWorldPoint, Time.deltaTime * 100);
    }

    /// <summary>
    /// Pokud je kliknuto na objekt, zaznamená se tato informace a dá do přední vrstvy.
    /// </summary>
    void OnMouseDown() {
        if (Clickable) {
            clicked = true;
            sr.sortingOrder++;
        }
    }

    /// <summary>
    /// Pokud hráč pustil objekt, zkontroluje se, zda byl umístěn na cílovou destinaci. Pokud ano,
    /// tak se do statistiky započítá správný, či nepsrávný pokus, podle toho, jestli tam měl být objekt umístěn.
    /// Dále pokud se s objektem pohlo jen kousek od jeho původní destinace, přečtou se názvy obrázky, které na něm jsou.
    /// </summary>
    void OnMouseUp() {
        if (Clickable) {
            clicked = false;
            sr.sortingOrder--;
            BoxCollider2D collider = destination.GetComponent<BoxCollider2D>();
            if (collider.OverlapPoint(transform.position)) {
                if (select) {
                    Destroy(gameObject);                    
                    Destroyed = true;
                    sc.PlayPraise();
                    sc.AddSuccess();
                    return;
                }
                else {
                    if (!failed) {                        
                        failed = true;                        
                        sc.PlayFail();
                        sc.AddFail();
                    }
                    return;
                }
            }
            collider = transform.GetComponent<BoxCollider2D>();
            if (collider.OverlapPoint(origin)) {
                Debug.Log("Reading names...");
                SoundManager.instance.PlaySingle(sound);
            }
        }
    }

 

    /// <summary>
    /// Přemístí objekt na jeho cílovou destinaci, pokud tam má být přemístěn (select == true).
    /// Toto slouží k případnému ukázání hráči, že na tento objekt zapomněl.
    /// </summary>
    /// <returns></returns>
    public bool ReachDestination() {
        
        if (select && !Destroyed) {
            sr.sortingOrder++;
            Destroyed = true;
            origin = destination.transform.position;
            timeMultiplicator = 1;
            return true;
        }

        return false;        
    }
}
