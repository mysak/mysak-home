﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController4 : WorksheetController {
    /// <summary>
    /// Maximální skóre v tomto worksheetu.
    /// </summary>
    public int max;
    /// <summary>
    /// Aktuální skóre hráče.
    /// </summary>
    private int score;

    private void Awake() {
        Init();
    }

    // Use this for initialization
    void Start () {
        score = 0;
	}

    /// <summary>
    /// Přidá úspěšný pokus ke statistice a automoticky zkontroluje skóre. Pokud už byly všechny objekty správně přemístěny, ukončí worksheet.
    /// </summary>
    /// <param name="weight">Váha daného pokusu, implicitně nastavena na 1.</param>
    public override void AddSuccess(int weight = 1) {
        score++;
        GameStat.AddSuccess(weight);
        if (score == max) {
            Debug.Log("Going to Praise And Exit");
            StartCoroutine(PraiseAndExit());    // aby se spustila pochvala, a pak teprve byl worksheet ukončen, je potřeba spustit nové unity vlákno
        }
    }

    /// <summary>
    /// Zvýší skóre a zkonstoluje, jestli nedosáhlo skóre maxima obdobně jako AddSucces. Ale nepřidá úspěšný pokus. Proto se používá, pokud uživatelův pokus byl neúspěšný, ale byl opraven logikou worksheetu.
    /// </summary>
    public void IncreaseScore() {
        score++;
        if (score == max) {
            SaveAndExit();
        }
    }


}
