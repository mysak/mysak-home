﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDrop4 : MonoBehaviour {

    /// <summary>
    /// Určuje, jestli je objekt právě přesouván uživatelem.
    /// </summary>
    private bool clicked = false;
    /// <summary>
    /// Vykresluje obrázek objektu.
    /// </summary>
    private SpriteRenderer sr;
    /// <summary>
    /// Na začátku původní pozice objektu, na který se má objekt vracet, pokus zrovna není přemisťován uživatelem.
    /// </summary>
    private Vector3 origin;
    /// <summary>
    /// Cílová destinace objektu
    /// </summary>
    public DragAndDrop4 destination;
    /// <summary>
    /// Controller tohoto worksheetu.
    /// </summary>
    public SceneController4 sc;
    /// <summary>
    /// Násobitel času, který určuje, jak rychle se bude objekt přesouvat na svůj origin. Čím větší, tím rychlejší.
    /// </summary>
    private int timeMultiplicator = 10;
    /// <summary>
    /// Určuje, jestli byl objekt chybně umístěn uživatelem na jiný objekt, než je ten cílový.
    /// </summary>
    private bool fail = false;
    /// <summary>
    /// Ostatní objekty v této scéně.
    /// </summary>
    private DragAndDrop4[] objects;
    /// <summary>
    /// Určuje, jestli cílový objekt byl špatně umístěn. Pokud ano, nechceme, abz se s tímto objektem hýbalo
    /// </summary>
    private bool destinationFailed = false;


    // Use this for initialization
    /// <summary>
    /// Nastaví se původní pozice objektu.
    /// </summary>
    void Start() {
        origin = transform.position;
        sr = GetComponent<SpriteRenderer>();
        objects = (DragAndDrop4[])FindObjectsOfType<DragAndDrop4>();
    }

    // Update is called once per frame
    /// <summary>
    /// Pokud zrovna hráč objekt drží, pohybuje se s prstem hráče.
    /// Jinak se objekt navrací na své původní místo.
    /// </summary>
    void Update() {
        if (clicked)
            Move();

        else {
            transform.position = Vector3.Lerp(transform.position, origin, Time.deltaTime * timeMultiplicator);

            BoxCollider2D collider = destination.GetComponent<BoxCollider2D>();
            if (fail && collider.OverlapPoint(transform.position)) {
                sc.IncreaseScore();
                Destroy(destination.gameObject);
                Destroy(gameObject);                
            }
        }
    }

    /// <summary>
    /// Pokud hráč klikne na objekt, dá se do popředí a zaznamená se tato událost.
    /// </summary>
    void OnMouseDown() {
        if (!fail && !destinationFailed) {
            clicked = true;
            sr.sortingOrder++;
        }
    }

    /// <summary>
    /// Pokud hráč objekt pustí, zkontroluje se, jestli je objekt na cílové destinaci. Pokud ano, připočítá se mu úspěch.
    /// Pokud je na špatné cílové destinaci, ukáže se mu správné řešení.
    /// V ostatních případech se nic neděje.
    /// </summary>
    void OnMouseUp() {
        clicked = false;        
        BoxCollider2D collider = destination.GetComponent<BoxCollider2D>();

        if (collider.OverlapPoint(transform.position)) {            
            sc.AddSuccess();
            sc.PlayPraise();
            Destroy(destination.gameObject);
            Destroy(gameObject);            
        }

        else {
            DragAndDrop4[] objects = (DragAndDrop4[])FindObjectsOfType<DragAndDrop4>();
            BoxCollider2D collider2;
            foreach (var o in objects) {
                collider2 = o.GetComponent<BoxCollider2D>();
                if (o != this && collider2.OverlapPoint(transform.position) && !(fail) && !(o.fail)) {
                    fail = true;
                    timeMultiplicator = 1;
                    destination.destinationFailed = true;
                    origin = transform.position;
                    StartCoroutine(PlayCorrection());
                    sc.AddFail();
                }
            }
        }
        if (!fail) {
            sr.sortingOrder--;
        }        
    }

    /// <summary>
    /// Přehraje hlášku opravy (ukázání správného řešení).
    /// </summary>
    /// <returns>Délka hlášky</returns>
    private IEnumerator PlayCorrection() {
        float time = sc.PlayCorrection();
        yield return new WaitForSeconds(time);
        origin = new Vector3(destination.transform.position.x, destination.transform.position.y);
    }

    /// <summary>
    /// Přemisťuje objekt s prstem hráče.
    /// </summary>
    public void Move() {
        Vector3 mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPoint.z = 0;
        transform.position = Vector3.Lerp(transform.position, mouseWorldPoint, Time.deltaTime * 100);
    }
}
