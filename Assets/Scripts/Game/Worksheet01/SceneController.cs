﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SceneController : WorksheetController {

    private int score;
    public int max;

    private void Awake() {
        Init();
    }
    /// <summary>
    /// Inicializace
    /// </summary>
    void Start () {
        score = 0;
	}
	
    /// <summary>
    /// Zvýšení skore
    /// </summary>
    public void IncreaseScore() {
        score++;
        if (score == max) 
            SaveAndExit();
    }

    /// <summary>
    /// Přidá úspěšný pokus ke statistice a automoticky zkontroluje skóre. Pokud už byly všechny objekty správně přemístěny, ukončí worksheet.
    /// </summary>
    /// <param name="weight">Váha daného pokusu, implicitně nastavena na 1.</param>
    public override void AddSuccess(int weight = 1) {
        score++;
        GameStat.AddSuccess(weight);
        if (score == max) {
            Debug.Log("Going to Praise And Exit");
            StartCoroutine(PraiseAndExit());    // aby se spustila pochvala, a pak teprve byl worksheet ukončen, je potřeba spustit nové unity vlákno
        }
    }
}
