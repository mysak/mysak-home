﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragandDrop : MonoBehaviour {

    /// <summary>
    /// Určuje, jestli je objekt právě přesouván uživatelem.
    /// </summary>
    private bool clicked = false;
    /// <summary>
    /// Vykresluje obrázek objektu.
    /// </summary>
    private SpriteRenderer sr;
    /// <summary>
    /// Na začátku původní pozice objektu, na který se má objekt vracet, pokus zrovna není přemisťován uživatelem.
    /// </summary>
    private Vector3 origin;
    /// <summary>
    /// Cílová destinace objektu
    /// </summary>
    public GameObject destination;
    /// <summary>
    /// Destinace, která pro tento objekt není cílová, 
    /// </summary>
    public GameObject failDestination;
    /// <summary>
    /// Controller tohoto worksheetu.
    /// </summary>
    public SceneController sc;
    /// <summary>
    /// Násobitel času, který určuje, jak rychle se bude objekt přesouvat na svůj origin. Čím větší, tím rychlejší.
    /// </summary>
    private int timeMultiplicator = 10;
    /// <summary>
    /// Určuje, jestli byl objekt chybně umístěn uživatelem na failDestination.
    /// </summary>
    private bool fail = false;


    // Use this for initialization
    /// <summary>
    /// Nastaví se původní pozice objektu.
    /// </summary>
    void Start () {
        origin = transform.position;
        sr = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
    /// <summary>
    /// Pokud zrovna hráč objekt drží, pohybuje se s prstem hráče.
    /// Jinak se objekt navrací na své původní místo.
    /// </summary>
	void Update () {
        if (clicked)
            Move();

        else {
            transform.position = Vector3.Lerp(transform.position, origin, Time.deltaTime * timeMultiplicator);

            BoxCollider2D collider = destination.GetComponent<BoxCollider2D>();
            if (fail && collider.OverlapPoint(transform.position)) {
                sc.IncreaseScore();
                Destroy(gameObject);                
            }
        }
    }

    /// <summary>
    /// Pokud hráč klikne na objekt, dá se do popředí a zaznamená se tato událost.
    /// </summary>
    void OnMouseDown()
    {
        if (!fail) {
            clicked = true;
            sr.sortingOrder++;
        }
    }

    /// <summary>
    /// Pokud hráč objekt pustí, zkontroluje se, jestli je objekt na cílové destinaci. Pokud ano, připočítá se mu úspěch.
    /// Pokud je na špatné cílové destinaci, ukáže se mu správné řešení.
    /// V ostatních případech se nic neděje.
    /// </summary>
    void OnMouseUp()
    {
        clicked = false;
        sr.sortingOrder--;
        BoxCollider2D collider = destination.GetComponent<BoxCollider2D>();
        BoxCollider2D collider2 = failDestination.GetComponent<BoxCollider2D>();
        if (collider.OverlapPoint(transform.position))
        {
            Destroy(gameObject);
            
            sc.AddSuccess();
            sc.PlayPraise();
            //    sc.IncreaseScore();            
        }
        else if (collider2.OverlapPoint(transform.position) && !fail) {
            fail = true;            
            origin = transform.position;            
            timeMultiplicator = 1;
            StartCoroutine(PlayCorrection());
            sc.AddFail();
        }
    }

    /// <summary>
    /// Přehraje hlášku opravy (ukázání správného řešení).
    /// </summary>
    /// <returns>Délka hlášky</returns>
    private IEnumerator PlayCorrection() {
        float time = sc.PlayCorrection();
        yield return new WaitForSeconds(time);
        origin = new Vector3(destination.transform.position.x, destination.transform.position.y);
    }

    /// <summary>
    /// Přemisťuje objekt s prstem hráče.
    /// </summary>
    public void Move() {
        Vector3 mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPoint.z = 0;
        transform.position = Vector3.Lerp(transform.position, mouseWorldPoint, Time.deltaTime * 100);
    }
}
