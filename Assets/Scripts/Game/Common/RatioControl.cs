﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
/// <summary>
/// Zdroj skriptu: http://2sa-studio.blogspot.com/2015/01/handling-aspect-ratio-in-unity2d.html
/// Připněte ke kameře v každé scéně, kde chcete zachovat požadované rozlišení. Skprit zachová původní rozměry obrázků a obrazovku případně doplní postraními černými pruhy.
/// </summary>

public class RatioControl : MonoBehaviour {

    /// <summary>
    /// Nastavte na požadované rozlišení. Např. pro rozlišení 16:9 dosadíte 16/9.
    /// </summary>
    public float targetAspect;

    /// <summary>
    /// Při spuštění scény přizpůsobí pohled kamery našemu požadovanému rozlišení.
    /// </summary>
    void Start() {
        float windowAspect = (float)Screen.width / (float)Screen.height;
        float scaleHeight = windowAspect / targetAspect;
        Camera camera = GetComponent<Camera>();

        if (scaleHeight < 1.0f) {
            camera.orthographicSize = camera.orthographicSize / scaleHeight;
        }
    }
}
