﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSounds : MonoBehaviour {
    /// <summary>
    /// Hlášky chválící za správné plnění úkolu.
    /// </summary>
    public AudioClip[] PraiseClips;
    /// <summary>
    /// Hlášky upozorňující, že hráč udělal chybu.
    /// </summary>
    public AudioClip[] FailClips;
    /// <summary>
    /// Hlášky upozorňující, že hráč udělal chybu a ukazující, jak to mělo být správně.
    /// </summary>
    public AudioClip[] CorrectionClips;

    /// <summary>
    /// Přehraje náhodnou pochvalu.
    /// </summary>
    public float PlayPraise() {        
        return SoundManager.instance.RandomizeSfx(PraiseClips);
    }

    /// <summary>
    /// Přehraje náhodné upozornění na chybu.
    /// </summary>
    /// <returns>Vrací délku přehrávaného klipu ve vteřinách.</returns>
    public float PlayFail() {
        return SoundManager.instance.RandomizeSfx(FailClips);
    }

    /// <summary>
    /// Přehraje náhodné opravení chyby.
    /// </summary>
    /// <returns>Vrací délku přehrávaného klipu ve vteřinách.</returns>
    public float PlayCorrection() {
        return SoundManager.instance.RandomizeSfx(CorrectionClips);
    }
}
