﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorksheetController : MonoBehaviour {

    /// <summary>Herní statistika</summary>
    protected Stat GameStat;
    /// <summary>Id worksheetu, který se právě hraje</summary>
    protected string WorksheetId;
    /// <summary>
    /// Odkaz na instanci třídy ScoreSounds, přes kterou se pouští hlášky společné pro všechny worksheety.
    /// </summary>
    public ScoreSounds ScoreSounds;
    /// <summary>
    /// Udává, jestli už byl hráč pochválen za správné řešení. Slouží k tomu, aby byl pochválen jen jednou, aby dabing nebyl otravný.
    /// </summary>
    private bool praised = false;
    
    /// <summary>
    /// This method must be called at the start of the game level, it is best to call it
    /// in the Awake method of derived Controller that takes care of the level.
    /// It initializes Stat and makes a time mark of level beginning.
    /// </summary>
    protected void Init() {
        Debug.Log("Init");
        WorksheetId = SceneManager.GetActiveScene().name;
        GameStat = new Stat(GameManager.instance.UserProfile.Id, WorksheetId);
    }

    /// <summary>
    /// Přidá do statistiky úspěch.
    /// </summary>
    /// <param name="weight">Váha pokusu.</param>
    virtual public void AddSuccess(int weight = 1)
    {
        GameStat.AddSuccess(weight);
    }

    /// <summary>
    /// Přidá do statistiky neúspěšný pokus.
    /// </summary>
    /// <param name="weight">Váha pokusu.</param>
    public void AddFail(int weight = 1)
    {
        GameStat.AddFail(weight);
    }

    /// <summary>
    /// Uloží statistiku a načte další scénu.
    /// </summary>
    virtual public void SaveAndExit() {
        GameManager gm = GameManager.instance;

        GameStat.Save();
        //if(GameManager.instance.UserData.IsPlayingScenario())
        if (gm.UserData.GetScenarioStatus() != UserData.ScenarioStatus.None)
            gm.UserData.CurrentScenario.AddStat(GameStat);

        gm.LoadNextWorksheet();
    }

    /// <summary>
    /// Přehraje pochvalu, pokud ještě přehrána nebyla. To znamená, že v každém worksheetu je pochvala přehrána nejvýše jednou, aby dabign nebyl otravný.
    /// </summary>
    /// <returns>Délka přehrávané hlášky.</returns>
    public float PlayPraise() {
        if (!praised) {            
            praised = true;
            return ScoreSounds.PlayPraise();
        }
        return 0;
    }


    /// <summary>
    /// Prostě jen přehraje pochvalovací hlášku, bez jakékoliv kontroly, jestli již byl pochválen. Jak název napovídá - používá se, pokud chceme uživatele na konci pochválit.
    /// </summary>
    /// <returns></returns>
    public float FinalPraise() {
        return ScoreSounds.PlayPraise();
    }


    /// <summary>
    /// Na konci worksheetu uživatele pochválí, a potom uloží statistiku a ukončí worksheet. Spouštějte Příkazem StartCoroutine(PraiseAndExit())
    /// </summary>
    /// <returns></returns>
    public IEnumerator PraiseAndExit() {
        Debug.Log("In PraiseAndExit");
        yield return new WaitForSeconds(FinalPraise());
        SaveAndExit();
    }

    /// <summary>
    /// Přehraje hlášku k opravení hráčovi chyby.
    /// </summary>
    /// <returns>Délka přehrávané hlášky.</returns>
    public float PlayCorrection() {
        return ScoreSounds.PlayCorrection();
    }

    /// <summary>
    /// Přehraje hlášku, která oznamuje, že hráč udělal chybu.
    /// </summary>
    /// <returns>Délka přehrávané hlášky.</returns>
    public float PlayFail() {
        return ScoreSounds.PlayFail();
    }

}
