﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helper : MonoBehaviour {

    /// <summary>
    /// Zvuková stopa, která se napojí za instrukcema. Informuje dítě, že pokud chce zopakovat zadání, má kliknout na prompter.
    /// </summary>
    public AudioClip repeat;
    /// <summary>
    /// Zadání worksheetu.
    /// </summary>
    public AudioClip instructions;

	/// <summary>
    /// Při načtení worksheetu se automaticky přehraje zadání.
    /// </summary>
	void Awake () {
        StartCoroutine(Start());
	}
	
    /// <summary>
    /// Pokud hráč klikne na ikonku nápovědy, přehraje se zadání.
    /// </summary>
    private void OnMouseUp() {
        SoundManager.instance.PlaySingle(instructions);
    }

    private IEnumerator Start() {
        SoundManager.instance.playCounter = 0;
        yield return new WaitForSeconds(SoundManager.instance.PlaySingle(instructions));
        if (SoundManager.instance.playCounter == 1)
            SoundManager.instance.PlaySingle(repeat);
    }

}
