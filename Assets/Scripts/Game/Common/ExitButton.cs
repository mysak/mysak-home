﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButton : MonoBehaviour {

    private const string sceneName = "UserSelect";

    private void OnMouseUp() {
        SoundManager.instance.StopSound();
        GameManager.instance.UserProfile = null;
        LoadScene loader = new LoadScene();
        loader.ChangeScene(sceneName);
    }

}
